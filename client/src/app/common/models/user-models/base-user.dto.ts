import { UserRole } from '../../enums/user-role.enum';

export class BaseUserDTO {
    public id: string;
    public username: string;
    public role: UserRole;
    public userProfileImgUrl: string;
}
