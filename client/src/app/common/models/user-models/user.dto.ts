import { UserRole } from 'src/app/common/enums/user-role.enum';

export class UserDTO {
  public id: string;
  public name: string;
  public username: string;
  public email: string;

  public role: UserRole;
}
