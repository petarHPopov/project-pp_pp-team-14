import { Vendor } from './Vendor.dto';
import { ProductName } from './ProductName.dto';

export class CPE {
    public id: string;
    public title: string;
    public version: string;
    public cpeUri23: string;
    public vendor: Vendor;
    public name: ProductName;
}
