import { ProductCVE } from './productCVE.dto';

export class ProductDTO {
  public id: string;
  public title: string;
  public name: string;
  public cpeUri23: string;
  public isAssignedCVE: string;
  public productCVEs: ProductCVE[];
}
