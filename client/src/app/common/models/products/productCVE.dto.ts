import { CVE } from './CVE.dto';

export class ProductCVE {
    public isAssigned: boolean;
    public cve: CVE;
}