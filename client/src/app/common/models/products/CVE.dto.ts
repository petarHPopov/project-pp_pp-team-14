export class CVE {
    public id: string;
    public cveId: string;
    public description: string;
    public lastModifiedDate: string;
}