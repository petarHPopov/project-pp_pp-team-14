import { Routes } from '@angular/router';
import { HomeComponent } from '../components/home/home.component';
import { AnonymousGuard } from '../core/guards/anonymous.guard';
import { LoginComponent } from '../components/login/login.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { RegisterComponent } from '../components/register/register.component';
import { AdminGuard } from '../core/guards/admin.guard';
import { ServerErrorComponent } from '../components/server-error/server-error.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { ListUserComponent } from '../shared/components/user/list-user/list-user.component';
import { ProductsComponent } from '../features/products/products.component';

export const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [AnonymousGuard] },

  { path: 'login', component: LoginComponent, canActivate: [AnonymousGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [AdminGuard] },

  {
    path: 'products', loadChildren: () => import('../features/products/products.module').then((m) => m.ProductsModule),
    canActivate: [AuthGuard],
  },

  { path: 'not-found', component: NotFoundComponent },
  { path: 'server-error', component: ServerErrorComponent },

  { path: '**', redirectTo: '/not-found' },
];

export const productsRoutes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    pathMatch: 'full',
  },

  {
    path: 'allUsers',
    component: ListUserComponent,
  },
];
