import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ProductsComponent } from './products.component';
import { ProductsRoutingModule } from './products-routing.module';
import { AgGridModule } from 'ag-grid-angular';
import { ProductCreate } from '../create-product/product-create.component';

@NgModule({
  declarations: [ProductsComponent, ProductCreate],
  imports: [
    SharedModule,
    ProductsRoutingModule,
    AgGridModule.withComponents([]),
  ],
})
export class ProductsModule {}
