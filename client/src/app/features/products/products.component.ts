import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductDTO } from '../../common/models/products/product.dto';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../../core/services/product.service';
import { ProductCVE } from '../../common/models/products/productCVE.dto';
import { Subscription, Observable } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit, OnDestroy {
  private routeSub: Subscription;
  public productsData: ProductDTO[];
  public productsDataWithCVE: ProductDTO[] = [];
  public productsDataWithoutCVE: ProductDTO[] = [];
  public selectedProductData: ProductCVE[] = [];
  private selectedProductId: string = '';
  public selectedIndex: number;

  public defaultColDef: any;
  public colResizeDefault: any;
  public allProuctsApi: any;
  public wCVEapi: any;
  public woCVEapi: any;
  public selectedApi: any;
  public selectProductTitle: string;

  public isAssignedCVE: boolean = false;

  // tslint:disable-next-line: typedef
  public columnDefsProducts = [
    { headerName: 'Title', field: 'title', filter: true, sortable: true },
    { headerName: 'Vendor', field: 'vendor', filter: true, width: 100, sortable: true },
    { headerName: 'Name', field: 'name', filter: true, width: 100, sortable: true },
    { headerName: 'Version', field: 'version', filter: true, width: 75, sortable: true },
    { headerName: 'cpeUri23', field: 'cpeUri23', width: 210, filter: true, sortable: true },
    {
      headerName: 'CVEs',
      field: 'productCVEs.length',
      width: 50,
      sortable: true,
      cellStyle: (params: any) => {
        const boolean = params.data.productCVEs.some((item: any) => item.isAssigned === true);
        if (params.data.productCVEs.length === 0) {
          return { textAlign: 'center' };
        } else if (boolean) {
          return { 'background-color': 'rgb(155, 218, 203)', textAlign: 'center' };
        } else {
          return { 'background-color': 'rgb(218, 155, 170)', textAlign: 'center' };
        }
      },
    },
  ];

  // tslint:disable-next-line: typedef
  public columnDefsWithCVE = [
    { headerName: 'Title', field: 'title', filter: true, width: 274, sortable: true },
    { headerName: 'Vendor', field: 'vendor', filter: true, width: 130, sortable: true },
    { headerName: 'Name', field: 'name', filter: true, width: 200, sortable: true },
    { headerName: 'Version', field: 'version', filter: true, width: 130, sortable: true },
    { headerName: 'cpeUri23', field: 'cpeUri23', width: 300, sortable: true },
    {
      headerName: 'CVE-s',
      field: 'productCVEs.length',
      width: 80,
      cellStyle: { textAlign: 'center' },
    },
  ];

  // tslint:disable-next-line: typedef
  public columnDefsWithoutCVE = [
    { headerName: 'Title', field: 'title', filter: true, width: 274, sortable: true },
    { headerName: 'Vendor', field: 'vendor', filter: true, width: 130, sortable: true },
    { headerName: 'Name', field: 'name', filter: true, width: 200, sortable: true },
    { headerName: 'Version', field: 'version', filter: true, width: 130, sortable: true },
    { headerName: 'cpeUri23', field: 'cpeUri23', width: 300, sortable: true },
    {
      headerName: 'CVE-s',
      field: 'productCVEs.length',
      width: 80,
      sortable: true,
      cellStyle: { textAlign: 'center' },
    },
  ];

  // tslint:disable-next-line: typedef
  public columnDefsSelectedProduct = [
    {
      headerName: '',
      field: 'isAssigned',
      width: 60,
      cellRenderer: (params: any) => {
        return `<input type='checkbox' ${params.value ? 'checked' : ''} />`;
      },
    },
    { headerName: 'CveId', field: 'cve.cveId', width: 130, sortable: true, filter: true },
    {
      headerName: 'LastModifiedDate',
      width: 160,
      field: 'cve.lastModifiedDate',
      filter: true,
      sortable: true
    },
    { headerName: 'Severity', field: 'cve.severity', width: 100, sortable: true },
    {
      headerName: 'Description',
      field: 'cve.description',
      width: 760,
      autoHeight: true,
      cellStyle: { 'white-space': 'normal' },
      sortable: true,
    },
  ];

  public constructor(
    private readonly route: ActivatedRoute,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly productService: ProductService
  ) {
    this.defaultColDef = { resizable: true };
    this.colResizeDefault = 'shift';
  }

  public ngOnInit(): void {
    this.routeSub = this.route.queryParams.subscribe(
      (params) => {
        if (params.search) {
          this.subscribeHandler(this.productService.search(params.search));
          this.selectedIndex = 0;
        } else if (params.reload) {
          this.router.navigate(['/products']);
          this.subscribeHandler(this.productService.getAllProducts());
        } else {
          this.subscribeHandler(this.productService.getAllProducts());
        }
      });
  }

  public ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  public onFirstDataRendered(params: any): void {
    this.allProuctsApi = params.api;
    this.allProuctsApi.sizeColumnsToFit();
  }

  public onFirstDataRenderedWCVE(params: any): void {
    this.wCVEapi = params.api;
  }

  public onFirstDataRenderedWoCVE(params: any): void {
    this.woCVEapi = params.api;
  }

  public onFirstDataRenderedSelected(params: any): void {
    this.selectedApi = params.api;
  }

  public onTagClick(event: any): void {
    switch (event.index) {
      case 0:
        this.selectedIndex = 0;
        break;
      case 1:
        this.wCVEapi?.sizeColumnsToFit();
        this.selectedIndex = 1;
        break;
      case 2:
        this.woCVEapi?.sizeColumnsToFit();
        this.selectedIndex = 2;
        break;
      case 3:
        this.selectedIndex = 3;
        setTimeout(() => {
          this.selectedApi?.sizeColumnsToFit();
          this.selectedApi?.resetRowHeights();
        }, 100);
        break;
    }
  }

  public onRowClicked(event: any): void {
    this.selectedProductData = event.data.productCVEs;
    this.selectedProductData.map(item => item.cve.lastModifiedDate = item.cve.lastModifiedDate.replace('T', ' ').replace('Z', ''));
    this.selectProductTitle = event.data.title;
    this.selectedProductId = event.data.id;
    this.selectedIndex = 3;
  }

  public onCellClickedCVE(event: any): void {

    if (event.colDef.headerName === '') {

      event.data.isAssigned = !event.data.isAssigned;
      event.api.refreshCells();
      this.productService.toggleAssignCVE(this.selectedProductId, event.data.cve.cveId)
        .subscribe((data) => {
          this.notificator.success(data.message);
          this.subscribeHandler(this.productService.getAllProducts());
        },
          () => {
            this.notificator.error(`CVE assign failed!`);
          });
    }
  }

  public onGridSizeChanged(event: any): void {
    if (event.clientWidth > 0) {
      event.api.sizeColumnsToFit();
      event.api.resetRowHeights();
    }
  }

  private subscribeHandler(observable: Observable<ProductDTO[]>): void {
    observable.subscribe({
      next: (data: ProductDTO[]) => {
        this.productsData = data;
        this.productsDataWithoutCVE = [];
        this.productsDataWithCVE = [];

        for (const product of data) {
          if (product.productCVEs.length !== 0) {
            if (product.productCVEs.every((e) => e.isAssigned !== true)) {
              product.isAssignedCVE = 'false';
              this.productsDataWithoutCVE = [
                ...this.productsDataWithoutCVE,
                product,
              ];
            } else {
              product.isAssignedCVE = 'true';
              this.productsDataWithCVE = [...this.productsDataWithCVE, product];
            }
          }
        }
      },
      error: () => this.router.navigate(['/server-error']),
    });
  }
}
