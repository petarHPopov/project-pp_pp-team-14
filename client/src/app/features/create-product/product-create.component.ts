import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { RegisterComponent } from '../../components/register/register.component';
import { ProductService } from '../../core/services/product.service';
import { Subject, Subscription } from 'rxjs';
import { Vendor } from '../../common/models/products/Vendor.dto';
import { Router } from '@angular/router';
import { checkVendorValidator } from '../../core/validators/checkVendor.validator';
import { ProductName } from '../../common/models/products/ProductName.dto';
import { checkNameValidator } from '../../core/validators/checkName.validator';
import { CPE } from '../../common/models/products/CPE.dto';
import { checkCPEValidator } from '../../core/validators/checkCPE.validator';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css'],
})
export class ProductCreate implements OnInit, OnDestroy {
  public vendorGroup: FormGroup;
  public nameGroup: FormGroup;
  public versionGroup: FormGroup;
  public cpeUri23Group: FormGroup;
  public isEditable: boolean = false;

  private vendorsSub: Subscription;
  private vendors: Map<string, boolean> = new Map<string, boolean>();
  public filteredVendors: Subject<string[]> = new Subject<string[]>();
  private namesSub: Subscription;
  private names: Map<string, boolean> = new Map<string, boolean>();
  public filteredNames: Subject<string[]> = new Subject<string[]>();
  public cpes: Map<string, string> = new Map<string, string>();
  public displayCpes: Subject<CPE[]> = new Subject<CPE[]>();

  public constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly dialogRef: MatDialogRef<RegisterComponent>,
    private readonly productService: ProductService,
    private readonly router: Router,

  ) { }

  public ngOnInit(): void {
    this.productService.checkProduct(`vendor=returnAllVendors`).subscribe(({
      next: (resp: Vendor[]) => {
        for (const item of resp) {
          this.vendors.set(item.name, true);
        }
      },
      error: () => this.router.navigate(['/server-error']),
    }));

    this.vendorGroup = this._formBuilder.group({
      vendor: ['', [Validators.required, checkVendorValidator(this.vendors)]],
    });
    this.nameGroup = this._formBuilder.group({
      name: ['', [Validators.required, checkNameValidator(this.names)]],
    });
    this.versionGroup = this._formBuilder.group({
      version: [''],
    });
    this.cpeUri23Group = this._formBuilder.group({
      cpeUri23: ['', [Validators.required, checkCPEValidator(this.cpes)]],
    });

    this.vendorsSub = this.vendorGroup.get('vendor').valueChanges.subscribe(data => {
      if (data !== '') {
        this.productService.checkProduct(`vendor=${data}`).subscribe(({
          next: (resp: Vendor[]) => {
            this.filteredVendors.next(resp.map(item => item.name));
          },
          error: () => this.router.navigate(['/server-error']),
        }));
      }
    }
    );

    this.namesSub = this.nameGroup.get('name').valueChanges.subscribe(data => {
      let checkQuery: string = `vendor=${this.vendorGroup.get('vendor').value}&`;
      checkQuery += data ? `partialName=${data}` : `name=returnAllNames`;
      this.productService.checkProduct(checkQuery).subscribe(({
        next: (resp: ProductName[]) => {
          this.filteredNames.next(resp.map(item => item.name));
        },
        error: () => this.router.navigate(['/server-error']),
      }));
    }
    );

  }

  public ngOnDestroy(): void {
    this.vendorsSub.unsubscribe();
    this.namesSub.unsubscribe();
  }

  public onClickCloseModal(): void {
    this.dialogRef.close();
  }

  public getCPEs(): void {
    let queryString = `vendor=${this.vendorGroup.get('vendor').value}&name=${this.nameGroup.get('name').value}`;
    if (this.versionGroup.get('version').value) {
      queryString += `&version=${this.versionGroup.get('version').value}`;
    }
    this.productService.checkProduct(queryString).subscribe(({
      next: (resp: CPE[]) => {
        this.displayCpes.next(resp);
        for (const item of resp) {
          this.cpes.set(item.cpeUri23, item.title);
        }
      },
      error: () => this.router.navigate(['/server-error']),
    }));
  }

  public getNames(): void {
    if (this.vendorGroup.get('vendor').value !== '') {
      this.productService.checkProduct(`vendor=${this.vendorGroup.get('vendor').value}&name=returnAllNames`)
        .subscribe(({
          next: (resp: ProductName[]) => {
            for (const item of resp) {
              this.names.set(item.name, true);
            }
            this.filteredNames.next(resp.map(item => item.name));
          },
          error: () => this.router.navigate(['/server-error']),
        }));
    }
  }

}
