import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  public loginForm: FormGroup;

  public constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(16)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(32)])]
    });
  }
  public login(): void {
    this.authService.login(this.loginForm.value).subscribe(
      () => {
        this.notificator.success(
          `Welcome back ${this.loginForm.value.username}!`
        );
        this.router.navigate(['/products']);
      },
      (e) => {
        this.notificator.error(`${e.error.error}`);
      }
    );
  }
}
