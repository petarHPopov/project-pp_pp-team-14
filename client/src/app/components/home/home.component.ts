import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../common/models/user-models/user.dto';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;

  public loggedIn: boolean;
  public user: UserDTO;
  public constructor(
    private readonly authService: AuthService,
  ) { }

  public ngOnInit(): void {
    this.loggedInSubscription = this.authService.isLoggedIn$.subscribe(
      loggedIn => this.loggedIn = loggedIn,
    );

    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }
  public ngOnDestroy(): void {
    this.loggedInSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }

}
