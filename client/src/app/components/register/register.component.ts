import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  public createUser: FormGroup;

  public constructor(
    private readonly formBuilder: FormBuilder,
    private readonly dialogRef: MatDialogRef<RegisterComponent>
  ) {
    this.createUser = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(35)])],
      username: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(16)])],
      email: ['', Validators.compose([Validators.required, Validators.email, Validators.minLength(10), Validators.maxLength(60)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(32)])]
    });
  }

  public onClickCloseModal(): void {
    this.dialogRef.close();
  }

}
