import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { JwtModule } from '@auth0/angular-jwt';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RegisterComponent } from './components/register/register.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { TOKEN_INTERCEPTOR, ERROR_INTERCEPTOR, UNAUTHORIZE_INTERCEPTOR } from './core/interceptors';
import { UserComponent } from './shared/components/user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NotFoundComponent,
    RegisterComponent,
    ServerErrorComponent,
    UserComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    AppRoutingModule,
    SharedModule,
    JwtModule.forRoot({ config: {} }),
  ],
  providers: [
    TOKEN_INTERCEPTOR,
    ERROR_INTERCEPTOR,
    UNAUTHORIZE_INTERCEPTOR,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
