import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { StorageService } from 'src/app/core/services/storage.service';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

    public constructor(
        private readonly storageService: StorageService,
    ) { }

    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = this.storageService.read('token') || '';

        let updatedRequest: any;

        if (request.url.includes('imgur')) {
            updatedRequest = request.clone({
                headers: request.headers.set('Authorization', `Client-ID 6e230461a6a04bb`)
            });
        } else {
            updatedRequest = request.clone({
                headers: request.headers.set('Authorization', `Bearer ${token}`)
            });
        }

        return next.handle(updatedRequest);
    }
}
