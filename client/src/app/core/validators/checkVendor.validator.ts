import { ValidatorFn, AbstractControl } from '@angular/forms';

export function checkVendorValidator(checkData: Map<string, boolean>): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean; } | null => {
        if (checkData !== undefined && !checkData.get(control.value)) {
            return { 'vendorIncluded': true };
        }
        return null;
    };
}