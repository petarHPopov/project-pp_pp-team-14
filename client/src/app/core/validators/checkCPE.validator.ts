import { ValidatorFn, AbstractControl } from '@angular/forms';

export function checkCPEValidator(checkData: Map<string, string>): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean; } | null => {
        if (checkData !== undefined && !checkData.get(control.value)) {
            return { 'cpeIncluded': true };
        }
        return null;
    };
}