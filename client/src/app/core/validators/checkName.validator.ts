import { ValidatorFn, AbstractControl } from '@angular/forms';

export function checkNameValidator(checkData: Map<string, boolean>): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean; } | null => {
        if (checkData !== undefined && !checkData.get(control.value)) {
            return { 'nameIncluded': true };
        }
        return null;
    };
}