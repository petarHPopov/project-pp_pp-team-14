import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AnonymousGuard implements CanActivate {
  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public canActivate(): Observable<boolean> {
    return this.authService.isLoggedIn$.pipe(
      tap((loggedIn) => {
        if (loggedIn) {
          this.router.navigate(['/products']);
        }
      }),
      map((loggedIn) => {
        if (!loggedIn) {
          return !loggedIn;
        }
      })
    );
  }
}
