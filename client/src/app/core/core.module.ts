import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { AuthService } from './services/auth.service';
import { NotificatorService } from './services/notificator.service';
import { StorageService } from './services/storage.service';
import { UsersService } from './services/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorInterceptorService } from './interceptors/error-interceptor.service';
import { ProductService } from './services/product.service';

@NgModule({
  providers: [
    AuthService,
    NotificatorService,
    StorageService,
    UsersService,
    ErrorInterceptorService,
    ProductService,
  ],
  declarations: [],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      countDuplicates: true,
    }),
  ],
  exports: [HttpClientModule],
})
export class CoreModule {}
