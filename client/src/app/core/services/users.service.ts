import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateUserDTO } from '../../common/models/user-models/create-user.dto';
import { UserDTO } from '../../common/models/user-models/user.dto';
import { Observable } from 'rxjs';
import { BaseUserDTO } from '../../common/models/user-models/base-user.dto';
import { AuthService } from './auth.service';
import { UserProfilePictureDTO } from '../../common/models/user-models/user-profile-picture.dto';
import { CONFIG } from '../../config/config';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  public constructor(private readonly http: HttpClient) {}

  public createUser(user: CreateUserDTO): Observable<UserDTO> {
    return this.http.post<UserDTO>(`${CONFIG.MAIN_URL}/users`, user);
  }
  public getAllUsers(): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(`${CONFIG.MAIN_URL}/users/`);
  }

  public alertAdmin(): Observable<{ message: string }> {
    return this.http.put<{ message: string }>(
      `${CONFIG.MAIN_URL}/users/alertAdmin`,
      {}
    );
  }

  public updateUserProfilePicture(
    img: UserProfilePictureDTO,
    userId: string
  ): Observable<BaseUserDTO> {
    return this.http.put<BaseUserDTO>(
      `${CONFIG.MAIN_URL}/users/${userId}`,
      img
    );
  }
}
