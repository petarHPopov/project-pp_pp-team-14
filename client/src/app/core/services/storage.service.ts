import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  public constructor() { }

  public save(key: string, value: any): void {
    localStorage.setItem(key, String(value));
  }

  public read(key: string): string {
    const value = localStorage.getItem(key);

    return value && value !== 'undefined'
      ? value
      : null;
  }

  public delete(key: string): void {
    localStorage.removeItem(key);
  }

  public clear(): void {
    localStorage.clear();
  }
}
