import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserSummaryService {

  public constructor(
    private readonly http: HttpClient,
  ) { }

  // tslint:disable-next-line: typedef
  public uploadImg(img: File) {
    return this.http.post(`https://api.imgur.com/3/image`, img);
  }
}
