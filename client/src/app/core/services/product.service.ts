import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductDTO } from '../../common/models/products/product.dto';
import { CONFIG } from '../../config/config';
import { Injectable } from '@angular/core';
import { Vendor } from '../../common/models/products/Vendor.dto';
import { ProductName } from '../../common/models/products/ProductName.dto';
import { CPE } from '../../common/models/products/CPE.dto';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  public constructor(private readonly http: HttpClient) { }

  public getAllProducts(): Observable<ProductDTO[]> {
    return this.http.get<ProductDTO[]>(`${CONFIG.MAIN_URL}/products/`);
  }

  public checkProduct(query: string): Observable<Vendor[] | ProductName[] | CPE[]> {
    return this.http.get<Vendor[] | ProductName[] | CPE[]>(`${CONFIG.MAIN_URL}/products/checkProduct?${query}`);
  }

  public createProduct(cpeUri23: string): Observable<ProductDTO[]> {
    return this.http.post<ProductDTO[]>(`${CONFIG.MAIN_URL}/products/`, cpeUri23);
  }

  public search(searchTerm: string): Observable<ProductDTO[]> {
    return this.http.get<ProductDTO[]>(`${CONFIG.MAIN_URL}/products?search=${searchTerm}`);
  }

  public update(key: string): Observable<{ message: string; }> {
    return this.http.put<{ message: string; }>(
      `${CONFIG.MAIN_URL}/products/${key}`,
      {}
    );
  }

  public toggleAssignCVE(productId: string, cveId: string): Observable<{ message: string; }> {
    return this.http.put<{ message: string; }>(
      `${CONFIG.MAIN_URL}/products/${productId}`,
      { cve: cveId }
    );
  }

}
