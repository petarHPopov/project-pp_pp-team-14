import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { __importDefault } from 'tslib';
import { UserDTO } from '../../../../common/models/user-models/user.dto';
import { UsersService } from '../../../../core/services/users.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css'],
})
export class ListUserComponent implements OnInit {
  public rowData: UserDTO[];
  private routeSub: Subscription;
  public defaultColDef: any;
  public colResizeDefault: any;

  // tslint:disable-next-line: typedef
  public columnDefs = [
    { headerName: 'Id', field: 'id', filter: true, width: 300, sortable: true },
    { headerName: 'Name', field: 'name', filter: true, width: 300, sortable: true },
    { headerName: 'Username', field: 'username', filter: true, width: 190, sortable: true },
    { headerName: 'Email', field: 'email', filter: true, width: 250, sortable: true },
    { headerName: 'Role', field: 'role', filter: true, width: 120, sortable: true },
  ];

  public constructor(
    private readonly usersService: UsersService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,

  ) {
    this.defaultColDef = { resizable: true };
    this.colResizeDefault = 'shift';
  }

  public ngOnInit(): void {
    this.routeSub = this.route.queryParams.subscribe((params) => {
      if (params.reload) {
        this.router.navigate(['/products/allUsers']);
        this.subscribeHandler(this.usersService.getAllUsers());
      } else {
        this.subscribeHandler(this.usersService.getAllUsers());
      }
    });
  }

  public ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  public onGridSizeChanged(event: any): void {
    if (event.clientWidth > 0) {
      event.api.sizeColumnsToFit();
      event.api.resetRowHeights();
    }
  }
  private subscribeHandler(observable: Observable<UserDTO[]>): void {
    observable.subscribe({
      next: (data: UserDTO[]) => this.rowData = data,
      error: () => this.router.navigate(['/server-error']),
    });
  }
}
