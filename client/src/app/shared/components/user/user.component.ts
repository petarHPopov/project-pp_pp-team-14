import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from '../../../core/services/users.service';
import { AuthService } from '../../../core/services/auth.service';
import { NotificatorService } from '../../../core/services/notificator.service';
import { Router } from '@angular/router';
import { UserSummaryService } from '../../../core/services/user-summary.service';
import { BaseUserDTO } from '../../../common/models/user-models/base-user.dto';
import { RegisterComponent } from '../../../components/register/register.component';
import { MatDialog } from '@angular/material/dialog';
import { ProductService } from '../../../core/services/product.service';
import { ProductCreate } from '../../../features/create-product/product-create.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  @Input() public user: BaseUserDTO;
  public searchString: string = '';

  public constructor(
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly userSummaryService: UserSummaryService,
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly productService: ProductService,
    private readonly dialog: MatDialog
  ) { }

  public ngOnInit(): void { }

  public logout(): void {
    this.authService.logout().subscribe(
      () => {
        this.notificator.success(`Successful logout!`);
        this.router.navigate(['/']);
      },
      () => {
        this.notificator.error(`Logout failed!`);
      }
    );
  }

  public search(): void {
    this.router.navigate(['/products'], {
      queryParams: { search: this.searchString },
    });
    this.searchString = '';
  }

  public createProduct(): void {
    const createModal = this.dialog.open(ProductCreate, {
      width: '1000px',
    });

    createModal.afterClosed().subscribe((product) => {
      if (product) {
        this.productService.createProduct(product.value).subscribe({
          next: () => {
            this.notificator.success(`Product successfully created!`);
            this.reloadProducts();
          },
          error: (err: any) => this.notificator.error(`${err.error.error}`)
        });
      }
    });
  }

  public reloadProducts(): void {
    this.router.navigate(['/products'], {
      queryParams: { reload: true },
    });
  }
  public reloadUsers(): void {
    this.router.navigate(['/products/allUsers'], {
      queryParams: { reload: true },
    });
  }

  public updateCPE(): void {
    this.productService.update('updateCPEs').subscribe(
      (data) => {
        this.notificator.success(data.message);
      },
      () => {
        this.notificator.error(`Update CPE failed!`);
      }
    );
  }

  public updateCVE(): void {
    this.productService.update('updateCVEs').subscribe(
      (data) => {
        this.notificator.success(data.message);
      },
      () => {
        this.notificator.error(`Update CVE failed!`);
      }
    );
  }

  public alertAdmin(): void {
    this.usersService.alertAdmin().subscribe(
      (data) => {
        this.notificator.success(data.message);
      },
      () => {
        this.notificator.error(`Alert failed!`);
      }
    );
  }

  public editUserImg(file: File): void {
    this.userSummaryService
      .uploadImg(file)
      .toPromise()
      .then((res: any) => {
        this.user.userProfileImgUrl = res.data.link;
        this.usersService
          .updateUserProfilePicture({ img: res.data.link }, this.user.id)
          .subscribe(() => {
            this.notificator.success(`Your photo has been changed successfully!`);
          });
      });
  }

  public openDialogRegister(): void {
    const userModal = this.dialog.open(RegisterComponent, {
      width: '350px',
    });

    userModal.afterClosed().subscribe((user) => {
      if (user) {
        this.usersService.createUser(user.value).subscribe(() => {
          this.notificator.success(`User created successful!`);
          this.reloadUsers();
        });
      }
    });
  }
}
