import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ListUserComponent } from './components/user/list-user/list-user.component';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [
    ListUserComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    NgxSpinnerModule,

    AgGridModule.withComponents([]),
  ],
  exports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    NgxSpinnerModule,
  ],
})
export class SharedModule { }
