import { async, TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { ProductService } from '../../../app/core/services/product.service';
import { ProductDTO } from '../../../app/common/models/products/product.dto';
import { CONFIG } from '../../../app/config/config';
import { of } from 'rxjs';
import { Vendor } from '../../../app/common/models/products/Vendor.dto';

describe('ProductService', () => {

  let http;
  let service: ProductService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    http = {
      get(): void { },
      post(): void { },
      put(): void { },
    };

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        JwtModule.forRoot({ config: {} }),
      ],
      providers: [
        HttpClient,
      ],
    })
      .overrideProvider(HttpClient, { useValue: http });

    // tslint:disable-next-line: deprecation
    service = TestBed.get(ProductService);
  }));

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createProduct should', () => {
    it('call http.post() once with correct parameters', () => {
      // Arrange
      const cpeUri23 = 'cpe';
      const url = `${CONFIG.MAIN_URL}/products/`;
      const spy = jest.spyOn(http, 'post');

      // Act
      service.createProduct(cpeUri23);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, cpeUri23);
    });

    it('http.post() return correct value', done => {
      // Arrange
      const cpeUri23 = 'cpe';
      const mockReturn: any = new ProductDTO();

      jest.spyOn(http, 'post').mockReturnValue(of(mockReturn));

      // Act & Assert
      service.createProduct(cpeUri23).subscribe(
        (result) => {
          expect(result).toEqual(mockReturn);

          done();
        });
    });
  });
  describe('getAllProducts should', () => {
    it('call http.get() once with correct parameters', () => {
      // Arrange
      const url = `${CONFIG.MAIN_URL}/products/`;
      const spy = jest.spyOn(http, 'get');

      // Act
      service.getAllProducts();

      // Arrange
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it('http.get() return correct value', done => {
      // Arrange
      const mockedProduct = [new ProductDTO()];
      jest.spyOn(http, 'get').mockReturnValue(of(mockedProduct));

      // Act & Arrange
      service.getAllProducts().subscribe(
        result => {
          expect(result).toEqual(mockedProduct);

          done();
        });

    });
  });
  describe('checkProduct should', () => {
    it('call http.get() once with correct parameters', () => {
      // Arrange
      const query = 'test';
      const url = `${CONFIG.MAIN_URL}/products/checkProduct?${query}`;
      const spy = jest.spyOn(http, 'get');

      // Act
      service.checkProduct(query);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it('http.get() return correct value', done => {
      // Arrange
      const query = 'test';
      const vendor = [new Vendor()];
      jest.spyOn(http, 'get').mockReturnValue(of(vendor));

      // Act & Arrange
      service.checkProduct(query).subscribe(
        result => {
          expect(result).toEqual(vendor);

          done();
        });
    });
  });
  describe('search should', () => {
    it('call http.get() once with correct parameters', () => {
      // Arrange
      const searchTerm = 'test';
      const url = `${CONFIG.MAIN_URL}/products?search=${searchTerm}`;
      const spy = jest.spyOn(http, 'get');

      // Act
      service.search(searchTerm);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it('http.get() return correct value', done => {
      // Arrange
      const query = 'test';
      const product = [new ProductDTO()];
      jest.spyOn(http, 'get').mockReturnValue(of(product));

      // Act & Arrange
      service.search(query).subscribe(
        result => {
          expect(result).toEqual(product);

          done();
        });
    });
  });
  describe('update should', () => {
    it('call http.put() once with correct parameters', () => {
      // Arrange
      const key = 'key';
      const url = `${CONFIG.MAIN_URL}/products/${key}`;
      const spy = jest.spyOn(http, 'put');

      // Act
      service.update(key);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, {});
    });
  });
  describe('toggleAssignCVE should', () => {
    it('call http.put() once with correct parameters', () => {
      // Arrange
      const productId = '';
      const cveId = '';

      const url = `${CONFIG.MAIN_URL}/products/${productId}`;
      const spy = jest.spyOn(http, 'put');

      // Act
      service.toggleAssignCVE(productId, cveId);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, { cve: cveId });
    });
  });
});
