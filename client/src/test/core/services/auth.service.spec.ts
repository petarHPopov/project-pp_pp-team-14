import { AuthService } from '../../../app/core/services/auth.service';
import { TestBed, async } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from '../../../app/core/services/storage.service';
import { combineLatest, of } from 'rxjs';
import { UserDTO } from '../../../app/common/models/user-models/user.dto';
import { LoggedUserInfoDTO } from '../../../app/common/models/user-models/logged-user-info.dto';
import { LoginUserDTO } from '../../../app/common/models/user-models/login-user.dto';
import { CONFIG } from '../../../app/config/config';

describe('AuthService', () => {
  let http;
  let storage;
  let jwtService;

  let service: AuthService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    http = {
      get(): void { },
      post(): void { },
      delete(): void { }
    };

    storage = {
      read(): void { },
      save(): void { },
      clear(): void { }
    };

    jwtService = {
      decodeToken(): void { }
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule, JwtModule.forRoot({ config: {} })],
      providers: [AuthService, StorageService]
    })
      .overrideProvider(HttpClient, { useValue: http })
      .overrideProvider(StorageService, { useValue: storage })
      .overrideProvider(JwtHelperService, { useValue: jwtService });

    // tslint:disable-next-line: deprecation
    service = TestBed.get(AuthService);
  }));

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('handle correctly invalid (or empty/none) tokens', done => {
    // Arrange
    jest.spyOn(jwtService, 'decodeToken').mockImplementation(() => null);

    // Act & Assert
    // tslint:disable-next-line: deprecation
    combineLatest(service.isLoggedIn$, service.loggedUser$).subscribe(
      ([loggedIn, loggedUser]) => {
        expect(loggedIn).toBe(false);
        expect(loggedUser).toBe(null);

        done();
      });
  });

  describe('loggedUserInfoValue$ should', () => {
    it('return correct value of loggedUserInfoSubject$', () => {
      // Arrange
      const newUser = new LoggedUserInfoDTO();
      (service as any).loggedUserInfoSubject$.next(newUser);

      // Act & Assert
      expect(service.loggedUserInfoValue$()).toEqual(newUser);
    });
  });

  describe('nextLoggedUserInfo$ should', () => {
    it('emit new value of loggedUserInfoSubject$', done => {
      // Arrange
      const newUserValue = new LoggedUserInfoDTO();

      // Act
      service.nextLoggedUserInfo$(newUserValue);

      // Assert
      service.loggedUserInfo$.subscribe(
        (value) => {
          expect(value).toEqual(newUserValue);

          done();
        });
    });
  });

  describe('login should', () => {
    it('call http.post() once with correct parameters', () => {
      // Arrange
      const token = '';
      const user = new LoginUserDTO();
      const url = `${CONFIG.MAIN_URL}/session`;

      const spy = jest.spyOn(http, 'post').mockReturnValue(of(token));

      // Act
      service.login(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, user);
    });

    it('http.post() return correct value', done => {
      // Arrange
      const token = { token: '' };
      const user = new LoginUserDTO();

      jest.spyOn(http, 'post').mockReturnValue(of(token));

      // Act & Assert
      service.login(user).subscribe(
        (result) => {
          expect(result).toEqual(token);

          done();
        });
    });

    it('call jwtService.decodeToken() once with correct parameters', done => {
      // Arrange
      const mockedToken = { token: 'token' };
      const user = new LoginUserDTO();

      jest.spyOn(http, 'post').mockReturnValue(of(mockedToken));
      const spy = jest.spyOn(jwtService, 'decodeToken');

      // Act & Assert
      service.login(user).subscribe(
        ({ token }) => {
          expect(spy).toHaveBeenCalledTimes(1);
          expect(spy).toHaveBeenCalledWith(token);

          done();
        });
    });

    it('call storage.save() once with correct parameters', done => {
      // Arrange
      const mockedToken = { token: 'token' };
      const user = new LoginUserDTO();

      jest.spyOn(http, 'post').mockReturnValue(of(mockedToken));
      const spy = jest.spyOn(storage, 'save');

      // Act & Assert
      service.login(user).subscribe(
        ({ token }) => {
          expect(spy).toHaveBeenCalledTimes(1);
          expect(spy).toHaveBeenCalledWith('token', token);

          done();
        });
    });

    it('change state on isLoggedInSubject$ and loggedUserSubject$ with correct values', done => {
      // Arrange
      const mockedToken = { token: 'token' };
      const userForLogin = new LoginUserDTO();
      const userForEmit = new UserDTO();
      userForEmit.id = 'test';

      jest.spyOn(http, 'post').mockReturnValue(of(mockedToken));
      jest.spyOn(jwtService, 'decodeToken').mockImplementation(() => userForEmit);
      jest.spyOn(http, 'get').mockReturnValue(of({}));

      // Act & Assert
      service.login(userForLogin).subscribe(
        () => {
          // tslint:disable-next-line: deprecation
          combineLatest(service.isLoggedIn$, service.loggedUser$).subscribe(
            ([loggedId, loggedUser]) => {
              expect(loggedId).toBe(true);
              expect(loggedUser).toBeDefined();

              done();
            });
        }
      );
    });

    it('call loggedUserInfo private method once with correct parameters', done => {
      // Arrange
      const mockedToken = { token: 'token' };
      const userForLogin = new LoginUserDTO();
      const userForEmit = new UserDTO();
      userForEmit.id = 'test';

      jest.spyOn(http, 'post').mockReturnValue(of(mockedToken));
      jest.spyOn(jwtService, 'decodeToken').mockImplementation(() => userForEmit);
      jest.spyOn(http, 'get').mockReturnValue(of({}));

      const spy = jest.spyOn((service as any), 'loggedUserInfo');

      // Act & Assert
      service.login(userForLogin).subscribe(
        () => {
          expect(spy).toHaveBeenCalledTimes(1);
          expect(spy).toHaveBeenCalledWith('test');

          done();
        }
      );
    });
  });

  describe('logout should', () => {
    it('call http.delete() once with correct parameters', () => {
      // Arrange
      const msg = { msg: '' };
      const url = `${CONFIG.MAIN_URL}/session`;
      const spy = jest.spyOn(http, 'delete').mockImplementation(() => of(msg));

      // Act
      service.logout();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it('http.delete() return correct value', done => {
      // Arrange
      const msg = { msg: '' };
      jest.spyOn(http, 'delete').mockImplementation(() => of(msg));

      // Act & Assert
      service.logout().subscribe(
        (result) => {
          expect(result).toEqual(msg);

          done();
        });
    });

    it('call storage.clear() once', done => {
      // Arrange
      const msg = { msg: '' };

      jest.spyOn(http, 'delete').mockImplementation(() => of(msg));
      const spy = jest.spyOn(storage, 'clear');

      // Act & Assert
      service.logout().subscribe(
        () => {
          expect(spy).toHaveBeenCalledTimes(1);

          done();
        });
    });

    it('change start on isLoggedInSubject$, loggedUserSubject$ and loggedUserInfoSubject$ with correct values', done => {
      // Arrange
      const msg = { msg: '' };
      const emptyEmittedUser = new LoggedUserInfoDTO();
      jest.spyOn(http, 'delete').mockImplementation(() => of(msg));

      // Act & Assert
      service.logout().subscribe(
        () => {
          // tslint:disable-next-line: deprecation
          combineLatest(service.isLoggedIn$, service.loggedUser$, service.loggedUserInfo$).subscribe(
            ([loggedId, loggedUser, loggedUserInfo]) => {
              expect(loggedId).toBe(false);
              expect(loggedUser).toBe(null);
              expect(loggedUserInfo).toEqual(emptyEmittedUser);

              done();
            });
        });
    });
  });
});
