import { UserSummaryService } from '../../../app/core/services/user-summary.service';
import { async, TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';

describe('UserSummaryService', () => {
  let http;

  let service: UserSummaryService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    http = {
      post(): void { }
    };

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      providers: [
        HttpClient,
        UserSummaryService,
      ],
    })
      .overrideProvider(HttpClient, { useValue: http });

    // tslint:disable-next-line: deprecation
    service = TestBed.get(UserSummaryService);
  }));

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('uploadImg should', () => {
    it('call http.post() once with correct parameters', () => {
      // Arrange
      const file = new File(['50'], 'test');
      const url = `https://api.imgur.com/3/image`;

      const spy = jest.spyOn(http, 'post');

      // Act
      service.uploadImg(file);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, file);
    });
  });
});
