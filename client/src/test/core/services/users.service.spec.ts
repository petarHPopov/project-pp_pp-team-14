import { UserProfilePictureDTO } from './../../../app/common/models/user-models/user-profile-picture.dto';
import { BaseUserDTO } from './../../../app/common/models/user-models/base-user.dto';
import { UsersService } from '../../../app/core/services/users.service';
import { async, TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { CreateUserDTO } from '../../../app/common/models/user-models/create-user.dto';
import { CONFIG } from '../../../app/config/config';
import { of } from 'rxjs';
import { UserDTO } from '../../../app/common/models/user-models/user.dto';

describe('UsersService', () => {
  let http;

  let service: UsersService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    http = {
      get(): void { },
      post(): void { },
      put(): void { },
    };

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        JwtModule.forRoot({ config: {} }),
      ],
      providers: [
        UsersService,
        HttpClient,
      ],
    })
      .overrideProvider(HttpClient, { useValue: http });

    // tslint:disable-next-line: deprecation
    service = TestBed.get(UsersService);
  }));

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createUser should', () => {
    it('call http.post() once with correct parameters', () => {
      // Arrange
      const user = new CreateUserDTO();
      const url = `${CONFIG.MAIN_URL}/users`;
      const spy = jest.spyOn(http, 'post');

      // Act
      service.createUser(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, user);
    });

    it('http.post() return correct value', done => {
      // Arrange
      const user = new CreateUserDTO();
      const mockReturn = new UserDTO();

      jest.spyOn(http, 'post').mockReturnValue(of(mockReturn));

      // Act & Assert
      service.createUser(user).subscribe(
        (result) => {
          expect(result).toEqual(mockReturn);

          done();
        });
    });
  });

  describe('getAllUsers should', () => {
    it('call http.get() once with correct parameters', () => {
      // Arrange
      const url = `${CONFIG.MAIN_URL}/users/`;
      const spy = jest.spyOn(http, 'get');

      // Act
      service.getAllUsers();

      // Arrange
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it('http.get() return correct value', done => {
      // Arrange
      const mockedUsers = [new UserDTO()];
      jest.spyOn(http, 'get').mockReturnValue(of(mockedUsers));

      // Act & Arrange
      service.getAllUsers().subscribe(
        result => {
          expect(result).toEqual(mockedUsers);

          done();
        });

    });
  });

  describe('updateUserProfilePicture should', () => {
    it('call http.put() once with correct parameters', () => {
      // Arrange
      const userId = '5';
      const url = `${CONFIG.MAIN_URL}/users/${userId}`;
      const img = new UserProfilePictureDTO();
      const spy = jest.spyOn(http, 'put');

      // Act
      service.updateUserProfilePicture(img, userId);

      // Arrange
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, img);
    });

    it('http.put() return correct value', done => {
      // Arrange
      const userId = '5';
      const img = new UserProfilePictureDTO();
      const baseUser = new BaseUserDTO();

      jest.spyOn(http, 'put').mockReturnValue(of(baseUser));

      // Act & Assert
      service.updateUserProfilePicture(img, userId).subscribe(
        result => {
          expect(result).toEqual(baseUser);

          done();
        });
    });
  });
  describe('alertAdmin should', () => {
    it('call http.put() once with correct parameters', () => {
      // Arrange
      const user = new CreateUserDTO();
      const url = `${CONFIG.MAIN_URL}/users/alertAdmin`;
      const spy = jest.spyOn(http, 'put');

      // Act
      service.alertAdmin();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, {});
    });
  });
});
