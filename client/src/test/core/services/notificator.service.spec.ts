import { NotificatorService } from '../../../app/core/services/notificator.service';
import { TestBed } from '@angular/core/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';

describe('NotificatorService', () => {

  const toastr = {
    success(): void { },
    warning(): void { },
    error(): void { },
  };

  let service: NotificatorService;

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [ToastrModule],
      providers: [NotificatorService]
    })
      .overrideProvider(ToastrService, { useValue: toastr });

    // tslint:disable-next-line: deprecation
    service = TestBed.get(NotificatorService);
  });

  it('success should call success', () => {

    const spy = jest.spyOn(toastr, 'success').mockImplementation(() => { });

    service.success('test');

    expect(toastr.success).toHaveBeenCalledTimes(1);

  });

  it('warn should call warn', () => {

    const spy = jest.spyOn(toastr, 'warning').mockImplementation(() => { });

    service.warn('test');

    expect(toastr.warning).toHaveBeenCalledTimes(1);

  });

  it('error should call error', () => {

    const spy = jest.spyOn(toastr, 'error').mockImplementation(() => { });

    service.error('test');

    expect(toastr.error).toHaveBeenCalledTimes(1);

  });

});
