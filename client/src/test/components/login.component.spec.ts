import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { LoginComponent } from '../../app/components/login/login.component';
import { SharedModule } from '../../app/shared/shared.module';
import { AuthService } from '../../app/core/services/auth.service';
import { NotificatorService } from '../../app/core/services/notificator.service';

describe('LoginComponent', () => {
  let authService;
  let notificatorService;
  let router;
  let formBuilder;

  let fixture: ComponentFixture<LoginComponent>;
  let component: LoginComponent;

  beforeEach(async(() => {
    jest.clearAllMocks();

    authService = {
      login(): void { }
    };

    notificatorService = {
      success(): void { },
      error(): void { }
    };

    router = {
      navigate(): void { }
    };

    formBuilder = {
      group(): void { }
    };

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, ReactiveFormsModule, SharedModule, BrowserAnimationsModule],
      declarations: [LoginComponent],
      providers: [AuthService, NotificatorService]
    })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(NotificatorService, { useValue: notificatorService })
      .overrideProvider(Router, { useValue: router })
      .overrideProvider(FormBuilder, { useValue: formBuilder })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
      });
  }));

  it('should be defined', () => {
    // Arrange, Act & Assert
    expect(component).toBeDefined();
  });

  describe('login should', () => {
    it('call authService.login() once with correct parameters', () => {
      // Arrange
      component.loginForm = new FormBuilder().group({
        username: '',
        password: '',
      });

      const spy = jest.spyOn(authService, 'login').mockImplementation(() => of(true));

      // Act
      component.login();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(component.loginForm.value);
    });

    it('call notificator.success() once with correct parameteres', () => {
      // Arrange
      component.loginForm = new FormBuilder().group({
        username: '',
        password: '',
      });

      const spy = jest.spyOn(notificatorService, 'success');
      jest.spyOn(authService, 'login').mockImplementation(() => of(true));

      // Act
      component.login();
      fixture.detectChanges();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(`Welcome back ${component.loginForm.value.username}!`);
    });

    it('call router.navigate() once with correct parameteres', () => {
      // Arrange
      component.loginForm = new FormBuilder().group({
        username: '',
        password: '',
      });

      const spy = jest.spyOn(router, 'navigate');
      jest.spyOn(authService, 'login').mockImplementation(() => of(true));

      // Act
      component.login();
      fixture.detectChanges();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(['/products']);
    });

    it('call notificator.error() after unsuccessful login once with correct parameteres', () => {
      // Arrange
      component.loginForm = new FormBuilder().group({
        username: '',
        password: '',
      });

      const spy = jest.spyOn(notificatorService, 'error');
      jest.spyOn(authService, 'login')
        .mockImplementation(() => throwError({ error: { error: 'Something happen...' } }));

      // Act
      component.login();
      fixture.detectChanges();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('Something happen...');
    });
  });
});
