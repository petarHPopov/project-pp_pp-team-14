# Project PP_PP - Team 14 - codename "SafeGuard"

## Description

Project PP_PP takes up on the task and creates an app, that alleviate system administrators work by handing the task of listing the installed software on each PC to the respective user.

## Installation

    git clone https://gitlab.com/petarHPopov/project-pp_pp-team-14.git

1 . Setup MySQL Database with new Schema. Due to character content of the CPE dictionary it is required Charset/Collation to
be **utf8mb4** and **utf8mb4_unicode_ci** respectively.
<br>

2 . Setup **.env** and **оrmconfig.json** files with desired credentials in Server folder. They need to be on root level in api folder where is **package.json** and other config files.

- **.env** file with your settings:

  ```typescript
       PORT=3000
       DB_TYPE=mysql
       DB_HOST=localhost
       DB_PORT=3306
       DB_USERNAME=root
       DB_PASSWORD=password
       DB_DATABASE_NAME=namedb
       EXPIRESIN=12h
       SECRETKEY=s3cr37k3y
       EMAIL_ID=alert
       EMAIL_PASS=alertPass

  ```

- **ormconfig.json** file with your settings:

  ```typescript
   {
      "type": "mysql",
     "host": "localhost",
     "port": 3306,
     "username": "root",
     "password": "password",
     "database": "namedb",
     "synchronize": false,
      "migrations": ["src/database/migration/*.ts"],
      "migrationsRun": true,
      "entities": ["src/database/entities/*.entity.ts"],
      "cli": {
         "entitiesDir": "src/database/entities",
         "migrationsDir": "src/database/migration"
      }
   }

  ```

3 . After files are setup go in terminal and run:

- **npm run startup** - this command does the following:

  - install all the dependencies (command should be run in the same folder where package.json is).
  - will execute migrations sequentially ordered by timestamp.
  - Will add one admin, download the current CPE Dictionary, extract the datа and load it up in the DB
  - will launch the application in dev mode.

- go grab a coffee, loading data in DB may take up to and hour:</br></br>
  <img src="./client/src/assets/Seed.png"/></br>

4 . Start the Client from it's folder:

- **ng serve**

5 . You can test each of the functions with all the written tests.

- **npm run majestic**

6 . Use Postman

- In order to test the API use the Postman tool. We provided a postman collection with Project features and commands.
