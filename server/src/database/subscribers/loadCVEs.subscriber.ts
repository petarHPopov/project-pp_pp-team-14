import fetch from 'node-fetch';
import { EntitySubscriberInterface, EventSubscriber, getConnection, InsertEvent, Repository } from 'typeorm';
import { CVE } from '../entities/cve.entity';
import { ProductCVE } from '../entities/product-cve.entity';
import { Product } from '../entities/product.entity';

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<Product> {

    public listenTo(): any {
        return Product;
    }

    public afterInsert(event: InsertEvent<Product>): void {
        try {
            fetch(
                `https://services.nvd.nist.gov/rest/json/cves/1.0?resultsPerPage=1000&cpeMatchString=${event.entity.cpe.cpeUri23}`)
                .then(response => response.json())
                .then(data => {
                    if (data.totalResults > 0) {
                        data.result.CVE_Items.forEach(async(item: any) => {
                            insertCVE(item, event.entity).catch(console.error);
                        });
                    }
                });
        } catch (error) {
            console.log(`CVEs not loaded, try again later!, Error code:${error.code}, message:${error.message}`);
        }
    }
}

export const insertCVE = async(cve: any, product: Product): Promise<void> => {
    const connection = getConnection();
    const cveRepo: Repository<CVE> = connection.manager.getRepository(
        CVE
    );
    const productCVERepo: Repository<ProductCVE> = connection.manager.getRepository(
        ProductCVE
    );
    let newProductCVE;

    const checkCVE = await cveRepo.findOne({ cveId: cve.cve.CVE_data_meta.ID });

    if (checkCVE) {
        newProductCVE = {
            productId: product.id,
            cveId: checkCVE.id,
        };
    } else {
        const newCVE = {
            cveId: cve.cve.CVE_data_meta.ID,
            description: cve.cve.description.description_data[0].value,
            severity: cve.impact.baseMetricV2.severity,
            lastModifiedDate: cve.lastModifiedDate
        };
        const savedCVE = await cveRepo.save(newCVE);
        newProductCVE = {
            productId: product.id,
            cveId: savedCVE.id,
        };
    }

    const checkProductCVE = await productCVERepo.findOne({
        where: {
            productId: newProductCVE.productId,
            cveId: newProductCVE.cveId
        }
    });

    if (!checkProductCVE) {
        await productCVERepo.save(newProductCVE);
    }
};
