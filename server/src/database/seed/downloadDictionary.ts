import admZip from 'adm-zip';
import axios from 'axios';
import * as fs from 'fs';
import * as path from 'path';
import * as readline from 'readline';
import { Stream } from 'stream';
import { CPE } from '../entities/cpe.entity';
import { ProductName } from '../entities/productName.entity';
import { Vendor } from '../entities/vendor.entity';
const AdmZip = admZip;

export const getData = async(): Promise<
  [Array<Partial<Vendor>>, Array<Partial<ProductName>>, Array<Partial<CPE>>, string]
> => {
  console.log(`${new Date().toString().substr(16, 8)} Start Downloading CPE Dictionary...`);
  const url =
    'https://nvd.nist.gov/feeds/xml/cpe/dictionary/official-cpe-dictionary_v2.3.xml.zip';
  const pathZip = path.resolve(__dirname, 'dictionary.zip');
  const pathXML = path.resolve(__dirname, 'official-cpe-dictionary_v2.3.xml');

  const writer = fs.createWriteStream(pathZip);

  (await downloader(url)).data.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on('finish', async() => {
      console.log(`${new Date().toString().substr(16, 8)} Download finsih!`);
      unzipper(pathZip, __dirname);
      resolve(await convertData(pathXML));
    });
    writer.on('error', reject);
  });
};

const unzipper = (inputPath: string, outputPath: string): void => {
  console.log(`${new Date().toString().substr(16, 8)} Unzipping...`);
  const zip = new AdmZip(inputPath);
  zip.extractAllTo(outputPath, true);
  console.log(`${new Date().toString().substr(16, 8)} Unzipping finish!`);
};

const convertData = async(
  inputFile: string,
): Promise<[Array<Partial<Vendor>>, Array<Partial<ProductName>>, Array<Partial<CPE>>, string]> => {

  const vendors: Set<string> = new Set();
  const names: Set<string> = new Set();
  const cpes: Set<any> = new Set();

  const outVendors: Array<Partial<Vendor>> = [];
  const outNames: Array<Partial<ProductName>> = [];
  const outCpes: Array<Partial<CPE>> = [];

  const instream = fs.createReadStream(inputFile);
  const outstream = new Stream();
  const rl = readline.createInterface(instream, outstream as any);

  let updateTime: string;
  let title: string;
  let cpeUri: string;
  let deprecated: boolean = false;
  // let counter = 0;

  console.log(`${new Date().toString().substr(16, 8)} Data convertion...`);

  rl.on(`line`, (line) => {
    // Break on Nth line
    // counter++;
    // if (counter > 100) {
    //   instream.destroy();
    // }

    if (line.includes('<timestamp>')) {
      updateTime = line.substring(15, line.length - 12);
    }

    if (line.includes('<title ')) {
      title = line.substring(28, line.length - 8);
    }

    if (line.includes('<cpe-23:cpe23-item')) {
      cpeUri = line.substring(29, line.length - 3);
      const cpeArr = cpeUri.split(':');
      if (cpeArr[2] === 'a' && !deprecated) {
        const vendor = cpeArr[3];
        const name = cpeArr[4];
        names.add(JSON.stringify({ name, vendor }));
        vendors.add(vendor);
      }
    }

    if (line.includes('deprecated="true"')) {
      deprecated = true;
    }

    if (line.includes(`</cpe-item>`) && !deprecated) {
      const cpeArr = cpeUri.split(':');
      if (cpeArr[2] === 'a') {
        cpes.add({ title, cpeUri23: cpeUri, version: cpeArr[5] });
        (title = ''), (cpeUri = '');
      }
    } else if (line.includes(`</cpe-item>`)) {
      deprecated = false;
    }
  });

  return new Promise((resolve, reject) => {
    instream.on('close', () => {
      console.log(`${new Date().toString().substr(16, 8)} Data convertion finish!`);
      vendors.forEach((item) => outVendors.push({ name: item }));
      names.forEach((item) => outNames.push(JSON.parse(item)));
      cpes.forEach((item) => outCpes.push(item));
      // tslint:disable-next-line: max-line-length
      console.log(`${new Date().toString().substr(16, 8)} Extracted:\nVendors: ${outVendors.length}\nProducts: ${outNames.length}\nCPE records: ${outCpes.length}`);
      resolve([outVendors, outNames, outCpes, updateTime]);
    });
    instream.on('error', reject);
  });
};

const downloader = (url: string) => {
  return axios({
    url,
    method: 'GET',
    responseType: 'stream',
  });
};

export const cleanUp = (): void => {
  const pathName = path.resolve(__dirname, 'dictionary.zip');
  const pathNameUnzipped = path.resolve(
    __dirname,
    'official-cpe-dictionary_v2.3.xml',
  );
  fs.unlinkSync(pathName);
  fs.unlinkSync(pathNameUnzipped);
  console.log(`${new Date().toString().substr(16, 8)} Temp Files deleted!`);
};
