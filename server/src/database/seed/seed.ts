import * as bcrypt from 'bcrypt';
import { createConnection, Like, Repository } from 'typeorm';
import { EntityType } from '../../common/enums/entity-type.enum';
import { UserRole } from '../../common/enums/user-role.enum';
import { CPE } from '../entities/cpe.entity';
import { ProductName } from '../entities/productName.entity';
import { Update } from '../entities/updates.entity';
import { User } from '../entities/user.entity';
import { Vendor } from '../entities/vendor.entity';
import { cleanUp, getData } from './downloadDictionary';

const seedAdmin = async(connection: any) => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);

    const admin = await userRepo.findOne({
        where: {
            username: 'Admin',
        },
    });

    if (admin) {
        console.log(`${new Date().toString().substr(16, 8)} The DB already has an admin!`);
        return;
    }
    const name = 'Petar';
    const username = 'Admin';
    const email = 'pp_pp@pppp.bg';
    const password = 'asdASD123';
    const hashedPassword = await bcrypt.hash(password, 10);

    const newAdmin: User = userRepo.create({
        name,
        username,
        email,
        password: hashedPassword,
        role: UserRole.Admin,
    });

    await userRepo.save(newAdmin);
    console.log(`${new Date().toString().substr(16, 8)} Seeded admin successfully!`);
};

export const seedDictionary = async(connection: any) => {
    const vendorRepo: Repository<Vendor> = connection.manager.getRepository(
        Vendor,
    );

    const productNameRepo: Repository<
        ProductName
    > = connection.manager.getRepository(ProductName);

    const cpeRepo: Repository<CPE> = connection.manager.getRepository(CPE);

    const updateRepo: Repository<Update> = connection.manager.getRepository(
        Update,
    );

    const seeded = await cpeRepo.findOne({
        cpeUri23: Like('%'),
    });

    if (seeded) {
        console.log(`${new Date().toString().substr(16, 8)} The DB is already seeded with data!`);
        return;
    }

    let seedVendors: Array<Partial<Vendor>>;
    let seedNames: Array<Partial<ProductName>>;
    let seedCPEs: Array<Partial<CPE>>;
    let updateTime: string;

    try {
        [seedVendors, seedNames, seedCPEs, updateTime] = await getData();
    } catch (e) {
        console.log(e);
    }

    console.log(`${new Date().toString().substr(16, 8)} Data seeding...`);

    const saveVendors = await vendorRepo.save(seedVendors, { chunk: 2000 });
    const vendorsMap = new Map<string, Vendor>(
        saveVendors.map(vendor => [vendor.name, vendor]),
    );

    const saveProductNames = await productNameRepo.save(
        seedNames.map(name => {
            name.vendor = vendorsMap.get((name.vendor as any));
            return name;
        }), {
        chunk: 2000,
    });

    const productNamesMap = new Map<string, ProductName>(
        saveProductNames.map(product => [product.name, product]),
    );

    await cpeRepo.save(
        seedCPEs.map(cpe => {
            const cpeUri23 = cpe.cpeUri23.split(':');
            cpe.vendor = vendorsMap.get(cpeUri23[3]);
            cpe.name = productNamesMap.get(cpeUri23[4]);
            return cpe;
        }),
        { chunk: 2000 },
    );

    await updateRepo.save([
        { entity: EntityType.CPE, lastUpdate: updateTime },
        { entity: EntityType.CVE, lastUpdate: updateTime },
    ]);

    cleanUp();
    console.log(`${new Date().toString().substr(16, 8)} Data seeded successfully!`);
};

const seed = async() => {
    console.log(`${new Date().toString().substr(16, 8)} Seed started!`);
    const connection = await createConnection();

    await seedAdmin(connection);
    await seedDictionary(connection);

    await connection.close();
    console.log(`${new Date().toString().substr(16, 8)} Seed completed!`);
};

seed().catch(console.error);
