import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CPE } from './cpe.entity';
import { ProductName } from './productName.entity';

@Entity('vendors')
export class Vendor {
  @Index('vendor-id')
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Index('vendor-name')
  @Column('nvarchar', { nullable: false, unique: true })
  public name: string;

  @OneToMany(() => CPE, cpe => cpe.vendor)
  public cpes: Promise<CPE[]>;

  @OneToMany(() => ProductName, productName => productName.vendor)
  public productNames: Promise<ProductName[]>;
}
