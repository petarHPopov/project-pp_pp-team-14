import { Expose } from 'class-transformer';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CVESeverity } from '../../common/enums/cve-severity.enum';
import { ProductCVE } from './product-cve.entity';

@Entity('cves')
export class CVE {
    @Expose()
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Expose()
    @Column('nvarchar', { nullable: false })
    public cveId: string;

    @Expose()
    @Column('text')
    public description: string;

    @Expose()
    @Column('enum', { enum: CVESeverity, nullable: false, default: CVESeverity.none })
    public severity: string;

    @Expose()
    @Column('nvarchar', { nullable: false })
    public lastModifiedDate: string;

    @OneToMany(() => ProductCVE, (productCve) => productCve.cve)
    public cveProducts: ProductCVE[];
}
