import { IsString, Matches } from 'class-validator';
import { Column, Entity, Index, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Product } from './product.entity';
import { ProductName } from './productName.entity';
import { Vendor } from './vendor.entity';

@Entity('cpes')
export class CPE {
  @Index('cpe-id')
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Index('cpe-title')
  @IsString()
  @Column('nvarchar', { nullable: false, default: 'unassigned' })
  public title: string;

  @Index('cpe-version')
  @Column('nvarchar', { nullable: false })
  public version: string;

  @Index('cpe-vendor-id')
  @ManyToOne(() => Vendor, vendor => vendor.cpes, { eager: true })
  public vendor: Vendor;

  @Index('cpe-name-id')
  @ManyToOne(() => ProductName, name => name.cpes, { eager: true })
  public name: ProductName;

  // tslint:disable-next-line: max-line-length
  @Matches(/cpe:2\.3:[aho\*\-](:(((\?*|\*?)([a-zA-Z0-9\-\._]|(\\[\\\*\?!"#$$%&'\(\)\+,\/:;<=>@\[\]\^`\{\|}~]))+(\?*|\*?))|[\*\-])){5}(:(([a-zA-Z]{2,3}(-([a-zA-Z]{2}|[0-9]{3}))?)|[\*\-]))(:(((\?*|\*?)([a-zA-Z0-9\-\._]|(\\[\\\*\?!"#$$%&'\(\)\+,\/:;<=>@\[\]\^`\{\|}~]))+(\?*|\*?))|[\*\-])){4}/, {
    message: 'not a valid CPE',
  })
  @Column('nvarchar', { nullable: false, unique: true })
  public cpeUri23: string;

  @OneToOne(() => Product, product => product.cpe)
  public product: Product;
}