import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CVE } from './cve.entity';
import { Product } from './product.entity';

@Entity()
@Index(['productId', 'cveId'], { unique: true })
export class ProductCVE {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('uuid')
    public productId: string;

    @Column('boolean', { default: false })
    public isAssigned: boolean;

    @ManyToOne(
        () => Product,
        product => product.productCVEs,
    )
    @JoinColumn({ name: 'productId' })
    public product: Product;

    @Column('uuid')
    public cveId: string;

    @ManyToOne(
        () => CVE,
        cve => cve.cveProducts, { eager: true }
    )
    @JoinColumn({ name: 'cveId' })
    public cve: CVE;
}