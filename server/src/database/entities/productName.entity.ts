import { Column, Entity, Index, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CPE } from './cpe.entity';
import { Vendor } from './vendor.entity';

@Entity('productnames')
@Index(['name', 'vendor'], { unique: true })
export class ProductName {
  @Index('name-id')
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Index('name-name')
  @Column('nvarchar', { nullable: false })
  public name: string;

  @Index('name-vendor-id')
  @ManyToOne(() => Vendor, vendor => vendor.productNames, { eager: true })
  public vendor: Vendor;

  @OneToMany(() => CPE, cpe => cpe.name)
  public cpes: Promise<CPE[]>;

}