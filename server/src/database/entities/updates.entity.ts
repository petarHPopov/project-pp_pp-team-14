import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { EntityType } from '../../common/enums/entity-type.enum';

@Entity('updates')
export class Update {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ nullable: false, default: () => 'CURRENT_TIMESTAMP', type: 'timestamp' })
    public lastUpdate: string;

    @Column('enum', { enum: EntityType, nullable: false, unique: true })
    public entity: EntityType;
}
