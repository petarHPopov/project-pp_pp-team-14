import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { UserRole } from '../../common/enums/user-role.enum';
import { Product } from './product.entity';

@Entity('users')
export class User {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('nvarchar', { nullable: false, unique: true })
    public name: string;

    @Column('nvarchar', { nullable: false, unique: true })
    public username: string;

    @Column('nvarchar', { nullable: false })
    public password: string;

    @Column('nvarchar', { nullable: false })
    public email: string;

    @Column('enum', { enum: UserRole, nullable: false, default: UserRole.User })
    public role: UserRole;

    @Column('nvarchar', {
        nullable: false,
        default: 'https://i.imgur.com/lXmfcoi.png',
    })
    public userProfileImgUrl: string;

    @Column('boolean', { default: false })
    public isDeleted: boolean;

    @Column({
        nullable: false,
        default: () => 'CURRENT_TIMESTAMP',
        type: 'timestamp',
    })
    public createTime: string;

    @Column({
        nullable: false,
        default: () => 'CURRENT_TIMESTAMP',
        type: 'timestamp',
    })
    public updateTime: string;

    @OneToMany(() => Product, product => product.user)
    public products: Product[];
}
