import {  Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CPE } from './cpe.entity';
import { ProductCVE } from './product-cve.entity';
import { User } from './user.entity';

@Entity('products')
export class Product {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({
        nullable: false,
        default: () => 'CURRENT_TIMESTAMP',
        type: 'timestamp',
    })
    public createTime: string;

    @OneToOne(() => CPE, cpe => cpe.product, { eager: true })
    @JoinColumn()
    public cpe: CPE;

    @ManyToOne(() => User, user => user.products)
    public user: User;

    @OneToMany(() => ProductCVE, (productCve) => productCve.product, { eager: true })
    public productCVEs: ProductCVE[];

}
