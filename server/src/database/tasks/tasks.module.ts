import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TasksService } from '../../database/tasks/tasks.service';
import { CPE } from '../entities/cpe.entity';
import { CVE } from '../entities/cve.entity';
import { Product } from '../entities/product.entity';
import { ProductName } from '../entities/productName.entity';
import { Update } from '../entities/updates.entity';
import { User } from '../entities/user.entity';
import { Vendor } from '../entities/vendor.entity';

@Module({
    imports: [TypeOrmModule.forFeature([CPE, CVE, Product, ProductName, Update, User, Vendor])],
    providers: [TasksService],
    controllers: [],
    exports: [TasksService],
})
export class TaskModule { }