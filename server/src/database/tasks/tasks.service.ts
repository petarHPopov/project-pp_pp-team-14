import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs';
import fetch from 'node-fetch';
import { Repository } from 'typeorm';
import { EntityType } from '../../common/enums/entity-type.enum';
import { CPE } from '../entities/cpe.entity';
import { CVE } from '../entities/cve.entity';
import { Product } from '../entities/product.entity';
import { ProductName } from '../entities/productName.entity';
import { Update } from '../entities/updates.entity';
import { User } from '../entities/user.entity';
import { Vendor } from '../entities/vendor.entity';
import { insertCVE } from '../subscribers/loadCVEs.subscriber';

@Injectable()
export class TasksService {

    public constructor(
        private readonly mailerService: MailerService,

        @InjectRepository(CPE)
        private readonly cpeRepository: Repository<CPE>,

        @InjectRepository(CVE)
        private readonly cveRepository: Repository<CVE>,

        @InjectRepository(Product)
        private readonly productRepository: Repository<Product>,

        @InjectRepository(ProductName)
        private readonly productNameRepository: Repository<ProductName>,

        @InjectRepository(Vendor)
        private readonly vendorRepository: Repository<Vendor>,

        @InjectRepository(User)
        private readonly userRepository: Repository<User>,

        @InjectRepository(Update)
        private readonly updateRepository: Repository<Update>,

    ) { }

    @Cron(CronExpression.EVERY_DAY_AT_4AM)
    public async updateCPEs(): Promise<void> {
        const date = await this.updateRepository.findOne({ where: { entity: EntityType.CPE } });
        const lastUpdate: string = JSON.stringify(date.lastUpdate).substring(1, 20);
        try {
            await fetch(
                `https://services.nvd.nist.gov/rest/json/cpes/1.0?resultsPerPage=1000&modStartDate=${lastUpdate}:000 Z`
            )
                .then(response => response.json())
                .then(async data => {
                    const cpes = data.result.cpes;
                    if (cpes.length > 0) {
                        for (const cpe of cpes) {
                            const foundCPE = await this.cpeRepository.findOne({ where: { cpeUri23: cpe.cpe23Uri } });
                            if (!foundCPE) {
                                const cpeUriArr = cpe.cpe23Uri.split(':');
                                if (cpeUriArr[2] === 'a') {
                                    const vendor = cpeUriArr[3];
                                    const name = cpeUriArr[4];
                                    const saveCPE = this.cpeRepository
                                        .create({ title: cpe.titles[0].title, cpeUri23: cpe.cpe23Uri, version: cpeUriArr[5] });

                                    const foundVendor = await this.vendorRepository.findOne({ where: { name: vendor } });
                                    const cpeVendor = foundVendor ? foundVendor : await this.vendorRepository.save({ name: vendor });
                                    const foundName = await this.productNameRepository.findOne({ where: { name, vendor: cpeVendor } });
                                    saveCPE.vendor = cpeVendor;
                                    // tslint:disable-next-line: max-line-length
                                    saveCPE.name = foundName ? foundName : await this.productNameRepository.save({ name, vendor: cpeVendor });
                                    await this.cpeRepository.save(saveCPE);

                                }
                            }
                        }
                    }
                    const nowTime = data.result.feedTimestamp + `:00`;
                    this.updateRepository.save({ ...date, lastUpdate: nowTime });
                });
        } catch (error) {
            console.error(error.message);

        }
    }

    @Cron(CronExpression.EVERY_DAY_AT_4AM)
    public async updateCVEs(): Promise<void> {

        const date = await this.updateRepository.findOne({ where: { entity: EntityType.CVE } });
        const lastUpdate: string = JSON.stringify(date.lastUpdate).substring(1, 20);

        await fetch(
            `https://services.nvd.nist.gov/rest/json/cves/1.0?resultsPerPage=1000&modStartDate=${lastUpdate}:000 Z`
        )
            .then(response => response.json())
            .then(async data => {

                const cves = data.result.CVE_Items;

                if (cves.length > 0) {
                    for (const cve of cves) {

                        const foundCVE = await this.cveRepository.findOne({ where: { cveId: cve.cve.CVE_data_meta.ID } });
                        if (foundCVE) {
                            foundCVE.description = cve.cve.description.description_data[0].value;
                            foundCVE.severity = cve.impact.baseMetricV2.severity;
                            foundCVE.lastModifiedDate = cve.lastModifiedDate;

                            await this.cveRepository.save(foundCVE);
                        }
                    }
                }

                const nowTime = data.result.CVE_data_timestamp + `:00`;
                this.updateRepository.save({ ...date, lastUpdate: nowTime });
            });
    }

    @Cron(CronExpression.EVERY_MINUTE)
    public async loadCVEs(): Promise<void> {
        let products = await this.productRepository.find();
        products = products.filter(item => item.productCVEs.length === 0);
        if (products.length === 0) {
            return;
        }
        for (const product of products) {
            try {
                await fetch(
                    `https://services.nvd.nist.gov/rest/json/cves/1.0?resultsPerPage=1000&cpeMatchString=${product.cpe.cpeUri23}`
                )
                    .then(response => response.json())
                    .then(data => {
                        if (data.totalResults > 0) {
                            data.result.CVE_Items.forEach(async (item: any) => {
                                await insertCVE(item, product).catch(console.error);
                            });
                        }
                    });
            } catch (error) {
                console.error(error.message);
            }
        }
    }

    @Cron(CronExpression.EVERY_DAY_AT_3PM)
    public async alertAdmin(): Promise<void> {
        const admin = await this.userRepository.findOne({
            where: {
                username: 'Admin',
            },
        });

        const attachment = JSON.parse(fs.readFileSync('./src/database/tasks/alert.json', 'utf8'));

        const isEmpty = (obj: any) => {
            for (const key in obj) {
                if (obj.hasOwnProperty(key)) {
                    return false;
                }
            }
            return true;
        };

        if (!isEmpty(attachment)) {
            this.mailerService.sendMail({
                to: admin.email,
                from: 'alert@nest.js',
                subject: 'New CVEs added!',
                text: `Hello Admin,\n\nyou have new CVEs assigned by your lovely users, check the attached file.\n\nHave a wonderful day!`,
                attachments: [
                    {
                        filename: 'alert.json',
                        path: './src/database/tasks/alert.json'
                    },
                ]
            })
                .catch((err) => {
                    console.log(err);
                }).then(() => fs.writeFileSync('./src/database/tasks/alert.json', JSON.stringify({})));

        }

    }
}
