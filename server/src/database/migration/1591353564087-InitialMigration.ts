// tslint:disable
import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1591353564087 implements MigrationInterface {
    name = 'InitialMigration1591353564087'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `cves` (`id` varchar(36) NOT NULL, `cveId` varchar(255) NOT NULL, `description` text NOT NULL, `severity` enum ('CRITICAL', 'HIGH', 'MEDIUM', 'LOW', 'UNASSIGNED') NOT NULL DEFAULT 'UNASSIGNED', `lastModifiedDate` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `product_cve` (`id` varchar(36) NOT NULL, `productId` varchar(255) NOT NULL, `isAssigned` tinyint NOT NULL DEFAULT 0, `cveId` varchar(255) NOT NULL, UNIQUE INDEX `IDX_baf2711c4a9f6e7c08300c6340` (`productId`, `cveId`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `users` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `role` enum ('User', 'Admin') NOT NULL DEFAULT 'User', `userProfileImgUrl` varchar(255) NOT NULL DEFAULT 'https://i.imgur.com/lXmfcoi.png', `isDeleted` tinyint NOT NULL DEFAULT 0, `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, UNIQUE INDEX `IDX_51b8b26ac168fbe7d6f5653e6c` (`name`), UNIQUE INDEX `IDX_fe0bb3f6520ee0469504521e71` (`username`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `products` (`id` varchar(36) NOT NULL, `cpeId` varchar(36) NULL, `userId` varchar(36) NULL, UNIQUE INDEX `REL_ad2658c8857c76dff09d18d9e2` (`cpeId`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `vendors` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, INDEX `vendor-id` (`id`), INDEX `vendor-name` (`name`), UNIQUE INDEX `IDX_83065ec2a2c5052786c122e95b` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `productnames` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `vendorId` varchar(36) NULL, INDEX `name-id` (`id`), INDEX `name-name` (`name`), INDEX `name-vendor-id` (`vendorId`), UNIQUE INDEX `IDX_9bb86fe906c25a3c8a012abad2` (`name`, `vendorId`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `cpes` (`id` varchar(36) NOT NULL, `title` varchar(255) NOT NULL DEFAULT 'unassigned', `version` varchar(255) NOT NULL, `cpeUri23` varchar(255) NOT NULL, `vendorId` varchar(36) NULL, `nameId` varchar(36) NULL, INDEX `cpe-id` (`id`), INDEX `cpe-title` (`title`), INDEX `cpe-version` (`version`), INDEX `cpe-vendor-id` (`vendorId`), INDEX `cpe-name-id` (`nameId`), UNIQUE INDEX `IDX_2876f2be040fa8736aa1f370ab` (`cpeUri23`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `updates` (`id` varchar(36) NOT NULL, `lastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `entity` enum ('CPE', 'CVE') NOT NULL, UNIQUE INDEX `IDX_08a5d91fd579d6cc5241e6700c` (`entity`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `product_cve` ADD CONSTRAINT `FK_8a1d11b768c2bf6ad66db19768b` FOREIGN KEY (`productId`) REFERENCES `products`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `product_cve` ADD CONSTRAINT `FK_51ca6ff3ef508561f66657b4573` FOREIGN KEY (`cveId`) REFERENCES `cves`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `products` ADD CONSTRAINT `FK_ad2658c8857c76dff09d18d9e21` FOREIGN KEY (`cpeId`) REFERENCES `cpes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `products` ADD CONSTRAINT `FK_99d90c2a483d79f3b627fb1d5e9` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `productnames` ADD CONSTRAINT `FK_628be15b5b6325c41ddf2810935` FOREIGN KEY (`vendorId`) REFERENCES `vendors`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `cpes` ADD CONSTRAINT `FK_d7807d5978eb1550067fa454705` FOREIGN KEY (`vendorId`) REFERENCES `vendors`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `cpes` ADD CONSTRAINT `FK_c5a5839a63667450937350787a1` FOREIGN KEY (`nameId`) REFERENCES `productnames`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `cpes` DROP FOREIGN KEY `FK_c5a5839a63667450937350787a1`", undefined);
        await queryRunner.query("ALTER TABLE `cpes` DROP FOREIGN KEY `FK_d7807d5978eb1550067fa454705`", undefined);
        await queryRunner.query("ALTER TABLE `productnames` DROP FOREIGN KEY `FK_628be15b5b6325c41ddf2810935`", undefined);
        await queryRunner.query("ALTER TABLE `products` DROP FOREIGN KEY `FK_99d90c2a483d79f3b627fb1d5e9`", undefined);
        await queryRunner.query("ALTER TABLE `products` DROP FOREIGN KEY `FK_ad2658c8857c76dff09d18d9e21`", undefined);
        await queryRunner.query("ALTER TABLE `product_cve` DROP FOREIGN KEY `FK_51ca6ff3ef508561f66657b4573`", undefined);
        await queryRunner.query("ALTER TABLE `product_cve` DROP FOREIGN KEY `FK_8a1d11b768c2bf6ad66db19768b`", undefined);
        await queryRunner.query("DROP INDEX `IDX_08a5d91fd579d6cc5241e6700c` ON `updates`", undefined);
        await queryRunner.query("DROP TABLE `updates`", undefined);
        await queryRunner.query("DROP INDEX `IDX_2876f2be040fa8736aa1f370ab` ON `cpes`", undefined);
        await queryRunner.query("DROP INDEX `cpe-name-id` ON `cpes`", undefined);
        await queryRunner.query("DROP INDEX `cpe-vendor-id` ON `cpes`", undefined);
        await queryRunner.query("DROP INDEX `cpe-version` ON `cpes`", undefined);
        await queryRunner.query("DROP INDEX `cpe-title` ON `cpes`", undefined);
        await queryRunner.query("DROP INDEX `cpe-id` ON `cpes`", undefined);
        await queryRunner.query("DROP TABLE `cpes`", undefined);
        await queryRunner.query("DROP INDEX `IDX_9bb86fe906c25a3c8a012abad2` ON `productnames`", undefined);
        await queryRunner.query("DROP INDEX `name-vendor-id` ON `productnames`", undefined);
        await queryRunner.query("DROP INDEX `name-name` ON `productnames`", undefined);
        await queryRunner.query("DROP INDEX `name-id` ON `productnames`", undefined);
        await queryRunner.query("DROP TABLE `productnames`", undefined);
        await queryRunner.query("DROP INDEX `IDX_83065ec2a2c5052786c122e95b` ON `vendors`", undefined);
        await queryRunner.query("DROP INDEX `vendor-name` ON `vendors`", undefined);
        await queryRunner.query("DROP INDEX `vendor-id` ON `vendors`", undefined);
        await queryRunner.query("DROP TABLE `vendors`", undefined);
        await queryRunner.query("DROP INDEX `REL_ad2658c8857c76dff09d18d9e2` ON `products`", undefined);
        await queryRunner.query("DROP TABLE `products`", undefined);
        await queryRunner.query("DROP INDEX `IDX_fe0bb3f6520ee0469504521e71` ON `users`", undefined);
        await queryRunner.query("DROP INDEX `IDX_51b8b26ac168fbe7d6f5653e6c` ON `users`", undefined);
        await queryRunner.query("DROP TABLE `users`", undefined);
        await queryRunner.query("DROP INDEX `IDX_baf2711c4a9f6e7c08300c6340` ON `product_cve`", undefined);
        await queryRunner.query("DROP TABLE `product_cve`", undefined);
        await queryRunner.query("DROP TABLE `cves`", undefined);
    }

}
