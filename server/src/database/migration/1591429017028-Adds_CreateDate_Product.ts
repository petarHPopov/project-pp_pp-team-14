// tslint:disable
import {MigrationInterface, QueryRunner} from "typeorm";

export class AddsCreateDateProduct1591429017028 implements MigrationInterface {
    name = 'AddsCreateDateProduct1591429017028'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `products` ADD `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `products` DROP COLUMN `createTime`", undefined);
    }

}
