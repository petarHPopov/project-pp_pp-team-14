import Joi = require('@hapi/joi');
import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AuthModule } from './auth/auth.module';
import { ProductsModule } from './features/products/products.module';
import { UsersModule } from './features/users/users.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        PORT: Joi.number().default(3000),
        DB_TYPE: Joi.string().required(),
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_USERNAME: Joi.string().required(),
        DB_PASSWORD: Joi.string().required(),
        DB_DATABASE_NAME: Joi.string().required(),
      }),
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async(configService: ConfigService) => ({
        type: configService.get('DB_TYPE'),
        host: configService.get('DB_HOST'),
        port: +configService.get('DB_PORT'),
        username: configService.get('DB_USERNAME'),
        password: configService.get('DB_PASSWORD'),
        database: configService.get('DB_DATABASE_NAME'),
        entities: ['dist/database/entities/*.entity.{js,ts}'],
        subscribers: ['dist/database/subscribers/*.subscriber.{js,ts}'],
        migrations: ['dist/database/migration/*.js'],
        synchronize: false,
        migrationsRun: true,
      } as TypeOrmModuleOptions),
    }),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    AuthModule,
    ProductsModule,
    UsersModule,
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async(configService: ConfigService) => ({
        transport: {
          host: configService.get('EMAIL_HOST'),
          port: 25,
          secure: false,
          tls: {
            secureProtocol: 'TLSv1_method',
            rejectUnauthorized: false
          },
          auth: {
            user: configService.get('EMAIL_ID'),
            pass: configService.get('EMAIL_PASS'),
          },
        },
      }),
    }),
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule { }
