import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import helmet = require('helmet');
import { join } from 'path';
import { AppModule } from './app.module';
import { MySystemErrorFilter } from './common/filters/my-error.filter';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.useStaticAssets(join(__dirname, '..', 'avatars'), { prefix: '/avatars' });

  app.use(helmet());
  app.enableCors();

  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));
  app.useGlobalFilters(new MySystemErrorFilter());

  await app.listen(+app.get(ConfigService).get('PORT'));
}
bootstrap();
