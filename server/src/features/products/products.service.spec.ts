import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import * as fs from 'fs';
import { In, Repository } from 'typeorm';
import { MockType, repositoryMockFactory } from '../../common/types/mock-type';
import { CPE } from '../../database/entities/cpe.entity';
import { CVE } from '../../database/entities/cve.entity';
import { ProductCVE } from '../../database/entities/product-cve.entity';
import { Product } from '../../database/entities/product.entity';
import { ProductName } from '../../database/entities/productName.entity';
import { User } from '../../database/entities/user.entity';
import { Vendor } from '../../database/entities/vendor.entity';
import { TasksService } from '../../database/tasks/tasks.service';
import { ProductShowDTO } from './models';
import { ProductsService } from './products.service';

describe('ProductsService', () => {
  let service: ProductsService;

  let productRepositoryMock: MockType<Repository<Product>>;
  let cpeRepositoryMock: MockType<Repository<CPE>>;
  let cveRepositoryMock: MockType<Repository<CVE>>;
  let productNameRepositoryMock: MockType<Repository<ProductName>>;
  let productCVERepositoryMock: MockType<Repository<ProductCVE>>;
  let vendorRepositoryMock: MockType<Repository<Vendor>>;
  const tasksService: Partial<TasksService> = {
    updateCVEs(): Promise<void> {
      return null;
    },
    updateCPEs(): Promise<void> {
      return null;
    }
  };

  const fakeProduct = new Product;
  fakeProduct.id = '1';
  const fakeReturnProduct = new ProductShowDTO;
  fakeReturnProduct.id = '2';
  const fakeCVE = new CVE;
  fakeCVE.id = '3';
  const fakeProductName = new ProductName;
  fakeProductName.id = '4';
  const fakeVendor = new Vendor;
  fakeVendor.id = '5';
  const fakeCPE = new CPE;
  fakeCPE.id = '6';
  fakeCPE.vendor = fakeVendor;
  fakeCPE.name = fakeProductName;
  const fakeUser = new User;
  fakeVendor.id = '7';
  const fakeProductCVE = new ProductCVE;
  const fakeProductToConvert = new Product;
  fakeProductToConvert.cpe = fakeCPE;
  const fakeProductFromConvert = new Product;

  beforeEach(async() => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductsService,
        {
          provide: getRepositoryToken(CPE),
          useFactory: repositoryMockFactory,
        },
        {
          provide: getRepositoryToken(CVE),
          useFactory: repositoryMockFactory,
        },
        {
          provide: getRepositoryToken(Product),
          useFactory: repositoryMockFactory,
        },
        {
          provide: getRepositoryToken(ProductName),
          useFactory: repositoryMockFactory,
        },
        {
          provide: getRepositoryToken(ProductCVE),
          useFactory: repositoryMockFactory,
        },
        {
          provide: getRepositoryToken(Vendor),
          useFactory: repositoryMockFactory,
        },
        {
          provide: TasksService,
          useValue: tasksService,
        },
      ],
    }).compile();

    service = module.get<ProductsService>(ProductsService);
    productRepositoryMock = module.get(getRepositoryToken(Product));
    cpeRepositoryMock = module.get(getRepositoryToken(CPE));
    cveRepositoryMock = module.get(getRepositoryToken(CVE));
    productNameRepositoryMock = module.get(getRepositoryToken(ProductName));
    productCVERepositoryMock = module.get(getRepositoryToken(ProductCVE));
    vendorRepositoryMock = module.get(getRepositoryToken(Vendor));

    fakeProductCVE.id = '8';
    fakeProductCVE.isAssigned = true;
    fakeProductCVE.cveId = '9';
    fakeProductCVE.cve = fakeCVE;
    fakeProductCVE.product = fakeProduct;
    fakeProductToConvert.productCVEs = [fakeProductCVE];
    fakeProductFromConvert.productCVEs = [{ isAssigned: true, cve: fakeCVE }] as any;

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(tasksService).toBeDefined();
    expect(productRepositoryMock).toBeDefined();
    expect(cpeRepositoryMock).toBeDefined();
    expect(cveRepositoryMock).toBeDefined();
    expect(productNameRepositoryMock).toBeDefined();
    expect(productCVERepositoryMock).toBeDefined();
    expect(vendorRepositoryMock).toBeDefined();
  });

  describe('Test method all()', () => {
    it('should return correct data when search query is missing', async() => {
      productRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([fakeProduct])
      }));
      productRepositoryMock.find.mockReturnValue([fakeProduct]);
      jest
        .spyOn(service, 'convertToShoWDTO')
        .mockReturnValue(fakeReturnProduct);

      const result = await service.all();

      expect(result).toEqual([fakeReturnProduct]);
    });

    it('should return correct data when search query is present and CVE is not found', async() => {
      productRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([fakeProduct])
      }));
      productRepositoryMock.find.mockReturnValue([fakeProduct]);
      cveRepositoryMock.findOne.mockReturnValue(undefined);
      jest
        .spyOn(service, 'convertToShoWDTO')
        .mockReturnValue(fakeReturnProduct);

      const result = await service.all(`test`);

      expect(result).toEqual([fakeReturnProduct]);
    });

    it('should return correct data when search query is present and CVE is found', async() => {
      productRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([fakeProduct])
      }));
      productRepositoryMock.find.mockReturnValue([fakeProduct]);
      cveRepositoryMock.findOne.mockReturnValue(fakeCVE);
      jest
        .spyOn(service, 'convertToShoWDTO')
        .mockReturnValue(fakeReturnProduct);

      const result = await service.all(`test`);

      expect(result).toEqual([fakeReturnProduct]);
    });

    it('should return correct data when search query is present and ProductRepo finds nothing', async() => {
      productRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([])
      }));
      cveRepositoryMock.findOne.mockReturnValue(fakeCVE);
      jest
        .spyOn(service, 'convertToShoWDTO')
        .mockReturnValue(fakeReturnProduct);

      const result = await service.all(`test`);

      expect(result).toEqual([]);
    });

    it('should call ProductRepo.find with correct data', async() => {
      productRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([fakeProduct])
      }));
      cveRepositoryMock.findOne.mockReturnValue(fakeCVE);
      jest
        .spyOn(service, 'convertToShoWDTO')
        .mockReturnValue(fakeReturnProduct);
      const test = productRepositoryMock.find.mockReturnValue([fakeProduct]);

      await service.all(`test`);

      expect(test).toBeCalledTimes(1);
      expect(test).toBeCalledWith({
        where: { id: In(['1']) },
        order: {
          createTime: 'DESC'
        }
      });
    });

  });

  describe('Test method checkProduct()', () => {
    it('should return correct data when vendor query is present and name query is `returnAllNames`', async() => {
      productNameRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([fakeProductName])
      }));

      const result = await service.checkProduct('returnAllNames', undefined, 'test', undefined);

      expect(result).toEqual([fakeProductName]);
    });

    it('should return correct data when vendor query is present and name query is not `returnAllNames`', async() => {
      const test = cpeRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([fakeCPE])
      }));

      const result = await service.checkProduct('test', undefined, 'test', undefined);

      expect(result).toEqual([fakeCPE]);
      expect(test).toHaveBeenCalledTimes(4);
    });

    it('should return correct data when vendor and partialName query are present', async() => {
      const test = productNameRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        take: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnValue(undefined),
        getMany: jest.fn().mockReturnValue([fakeProduct])
      }));

      const result = await service.checkProduct(undefined, 'test', 'test', undefined);

      expect(result).toEqual([fakeProduct]);
      expect(test).toHaveBeenCalledTimes(3);
    });

    it('should return correct data when vendor and partialName query are present and there is exact match', async() => {
      const test = productNameRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        take: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnValue(fakeProduct),
        getMany: jest.fn().mockReturnValue([])
      }));

      const result = await service.checkProduct(undefined, 'test', 'test', undefined);

      expect(result).toEqual([fakeProduct]);
      expect(test).toHaveBeenCalledTimes(3);
    });

    it('should return correct data when only vendor query is present', async() => {
      const test = jest.spyOn(vendorRepositoryMock, 'find').mockReturnValue([fakeVendor]);

      const result = await service.checkProduct(undefined, undefined, 'test', undefined);

      expect(result).toEqual([fakeVendor]);
      expect(test).toHaveBeenCalledTimes(2);
    });

    it('should return correct data when only vendor query is present and it is `returnAllNames`', async() => {
      jest.spyOn(vendorRepositoryMock, 'find').mockReturnValue([fakeVendor]);

      const result = await service.checkProduct(undefined, undefined, 'returnAllVendors', undefined);

      expect(result).toEqual([fakeVendor]);
    });

    it('should return correct data when only name query is present', async() => {
      const test = jest.spyOn(productNameRepositoryMock, 'find').mockReturnValue([fakeProductName]);

      const result = await service.checkProduct('test', undefined, undefined, undefined);

      expect(result).toEqual([fakeProductName]);
      expect(test).toHaveBeenCalledTimes(2);
    });

    it('should return correct data when only name query is present and it is `returnAllNames`', async() => {
      jest.spyOn(productNameRepositoryMock, 'find').mockReturnValue([fakeProductName]);

      const result = await service.checkProduct('returnAllNames', undefined, undefined, undefined);

      expect(result).toEqual([fakeProductName]);
    });

    it('should return correct data when any other combination of queries is attempted', async() => {
      jest.spyOn(vendorRepositoryMock, 'find').mockReturnValue([fakeProductName]);

      const result = await service.checkProduct(undefined, 'test', undefined, 'test');

      expect(result).toEqual([]);
    });

  });

  describe('Test method create()', () => {
    it('should return correct data', async() => {
      productRepositoryMock.findOne.mockReturnValue(undefined);
      jest
        .spyOn(service, 'convertToShoWDTO')
        .mockReturnValue(fakeReturnProduct);
      jest
        .spyOn(productRepositoryMock, 'save')
        .mockReturnValue(fakeProduct);

      const result = await service.create(fakeCPE, fakeUser);

      expect(result).toEqual(fakeReturnProduct);
    });

    it('should throw an error with correct message when ProductRepo finds something', async() => {
      productRepositoryMock.findOne.mockReturnValue(fakeProduct);
      jest
        .spyOn(service, 'convertToShoWDTO')
        .mockReturnValue(fakeReturnProduct);
      jest
        .spyOn(productRepositoryMock, 'save')
        .mockReturnValue(fakeProduct);
      let test: string;

      try {
        await service.create(fakeCPE, fakeUser);
      } catch (e) {
        test = e.message;
      }

      expect(test).toBe(`Product with such CPE already exist, please try something else !`);
    });

    it('should call productRepository.findOne method with correct data', async() => {
      const test = productRepositoryMock.findOne.mockReturnValue(undefined);
      jest
        .spyOn(service, 'convertToShoWDTO')
        .mockReturnValue(fakeReturnProduct);
      jest
        .spyOn(productRepositoryMock, 'save')
        .mockReturnValue(fakeProduct);

      await service.create(fakeCPE, fakeUser);

      expect(test).toBeCalledTimes(1);
      expect(test).toBeCalledWith({
        where: { cpe: fakeCPE }
      });
    });

    it('should call productRepository.save method with correct data', async() => {
      productRepositoryMock.findOne.mockReturnValue(undefined);
      jest
        .spyOn(service, 'convertToShoWDTO')
        .mockReturnValue(fakeReturnProduct);
      const test = jest
        .spyOn(productRepositoryMock, 'save')
        .mockReturnValue(fakeProduct);

      await service.create(fakeCPE, fakeUser);

      expect(test).toBeCalledTimes(1);
      expect(test).toBeCalledWith({
        cpe: fakeCPE,
        user: fakeUser,
        productCVEs: [],
      });
    });

  });

  describe('Test method toggleAssignCVE()', () => {
    it('should return correct data', async() => {
      productCVERepositoryMock.findOne.mockReturnValue(fakeProductCVE);
      jest
        .spyOn(productCVERepositoryMock, 'save')
        .mockReturnValue(fakeProductCVE);
      jest
        .spyOn(JSON, 'parse')
        .mockReturnValue({});
      jest
        .spyOn(fs, 'writeFileSync').mockReturnThis;

      const result = await service.toggleAssignCVE(fakeProduct, fakeCVE);

      expect(result).toEqual({ message: 'CVE assign toggled successfully!' });
    });

    it('should throw an error with correct when productCVERepositoryMock.findOne returns nothing', async() => {
      productCVERepositoryMock.findOne.mockReturnValue(undefined);
      jest
        .spyOn(productCVERepositoryMock, 'save')
        .mockReturnValue(fakeProductCVE);
      jest
        .spyOn(JSON, 'parse')
        .mockReturnValue({ test: fakeProductCVE });
      jest
        .spyOn(fs, 'writeFileSync').mockReturnThis;

      let test: string;

      try {
        await service.toggleAssignCVE(fakeProduct, fakeCVE);
      } catch (e) {
        test = e.message;
      }

      expect(test).toBe(`Product doesn't have such CVE or no such Product found!`);
    });

    it('should return correct data when found ProductCVE isAssigned property is false', async() => {
      fakeProductCVE.isAssigned = false;
      productCVERepositoryMock.findOne.mockReturnValue(fakeProductCVE);
      jest
        .spyOn(productCVERepositoryMock, 'save')
        .mockReturnValue(fakeProductCVE);
      jest
        .spyOn(JSON, 'parse')
        .mockReturnValue({ test: fakeProductCVE });
      jest
        .spyOn(fs, 'writeFileSync').mockReturnThis;

      const result = await service.toggleAssignCVE(fakeProduct, fakeCVE);

      expect(result).toEqual({ message: 'CVE assign toggled successfully!' });
    });

  });

  describe('Test method updateCVEs()', () => {
    it('should return correct data', async() => {

      const result = await service.updateCVEs();

      expect(result).toEqual({ message: 'CVEs updated successfully!' });
    });

    it('should call taskService.updateCVEs method', async() => {
      const test = jest.spyOn(tasksService, 'updateCVEs');

      await service.updateCVEs();

      expect(test).toBeCalledTimes(1);
    });

  });

  describe('Test method updateCPEs()', () => {
    it('should return correct data', async() => {

      const result = await service.updateCPEs();

      expect(result).toEqual({ message: 'CPEs updated successfully!' });
    });

    it('should call taskService.updateCVEs method', async() => {
      const test = jest.spyOn(tasksService, 'updateCPEs');

      await service.updateCPEs();

      expect(test).toBeCalledTimes(1);
    });

  });

  describe('Test method convertToShoWDTO()', () => {
    it('should return correct data', async() => {

      const result = await service.convertToShoWDTO(fakeProductToConvert);

      expect(result).toEqual(fakeProductFromConvert);
    });

  });

});
