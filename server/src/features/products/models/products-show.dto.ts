import { Expose } from 'class-transformer';
import { CVE } from '../../../database/entities/cve.entity';

export class ProductShowDTO {
  @Expose()
  public id: string;

  @Expose()
  public title: string;

  @Expose()
  public vendor: string;

  @Expose()
  public name: string;

  @Expose()
  public version: string;

  @Expose()
  public cpeUri23: string;

  @Expose()
  public productCVEs: CVE[];
}