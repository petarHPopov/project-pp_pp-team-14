import { IsOptional, MaxLength, MinLength } from 'class-validator';

export class ProductCheckQueryDTO {

  @IsOptional()
  @MinLength(1)
  @MaxLength(50)
  public name: string;

  @IsOptional()
  @MinLength(1)
  @MaxLength(50)
  public partialName: string;

  @IsOptional()
  @MinLength(1)
  @MaxLength(50)
  public version: string;

  @IsOptional()
  @MinLength(1)
  @MaxLength(50)
  public vendor: string;

}
