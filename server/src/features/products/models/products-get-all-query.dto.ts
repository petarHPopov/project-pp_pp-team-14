import { IsOptional, MaxLength, MinLength } from 'class-validator';

export class ProductGetAllQueryDTO {

  @IsOptional()
  @MinLength(1)
  @MaxLength(50)
  public search: string;

}
