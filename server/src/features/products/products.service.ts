import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import * as fs from 'fs';
import { In, Like, Repository } from 'typeorm';
import { MySystemError } from '../../common/exceptions/my-system.error';
import { CPE } from '../../database/entities/cpe.entity';
import { CVE } from '../../database/entities/cve.entity';
import { ProductCVE } from '../../database/entities/product-cve.entity';
import { Product } from '../../database/entities/product.entity';
import { ProductName } from '../../database/entities/productName.entity';
import { User } from '../../database/entities/user.entity';
import { Vendor } from '../../database/entities/vendor.entity';
import { TasksService } from '../../database/tasks/tasks.service';
import { ProductShowDTO } from './models/products-show.dto';

@Injectable()
export class ProductsService {
  public constructor(
    @InjectRepository(CPE)
    private readonly cpeRepository: Repository<CPE>,

    @InjectRepository(CVE)
    private readonly cveRepository: Repository<CVE>,

    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,

    @InjectRepository(ProductName)
    private readonly productNameRepository: Repository<ProductName>,

    @InjectRepository(ProductCVE)
    private readonly productCVERepository: Repository<ProductCVE>,

    @InjectRepository(Vendor)
    private readonly vendorRepository: Repository<Vendor>,
    private readonly taskService: TasksService
  ) { }

  public async all(search?: string | undefined): Promise<ProductShowDTO[]> {
    let query = this.productRepository
      .createQueryBuilder('product');

    if (search) {
      const searchCVE = await this.cveRepository.findOne({ where: { cveId: search } });

      if (searchCVE) {
        query = query.innerJoinAndSelect('product.productCVEs', 'productCVE', 'productCVE.cveId = :cveId', { cveId: searchCVE.id });
      } else {
        query = query.innerJoinAndSelect('product.cpe', 'cpe', 'cpe.title  like :search', { search: `%${search}%` });
      }
    }

    const products: any = await query.getMany();

    const productIds = products.map((product: Product) => product.id);

    if (productIds.length === 0) {
      return [];
    }

    const foundProducts: any = await this.productRepository.find({
      where: { id: In(productIds) },
      order: {
        createTime: 'DESC'
      }
    });

    return foundProducts.map((product: Product) => this.convertToShoWDTO(product));
  }

  public async checkProduct(
    name?: string | undefined,
    partialName?: string | undefined,
    vendor?: string | undefined,
    version?: string | undefined
  ): Promise<Vendor[] | ProductName[] | CPE[]> {

    if (vendor && name === 'returnAllNames') {

      return await this.productNameRepository
        .createQueryBuilder('productName')
        .innerJoinAndSelect('productName.vendor', 'vendor', 'vendor.name = :vendor', { vendor })
        .getMany();

    } else if (vendor && name) {

      const response: Set<string> = new Set();

      const fullMatch = await this.cpeRepository
        .createQueryBuilder('cpe')
        .innerJoinAndSelect('cpe.vendor', 'vendor', 'vendor.name = :vendor', { vendor })
        .innerJoinAndSelect('cpe.name', 'name', 'name.name = :name', { name })
        .where('cpe.version like :search', { search: version })
        .getMany();

      fullMatch.forEach(i => response.add(JSON.stringify(i)));

      const begginingVersionMatch = await this.cpeRepository
        .createQueryBuilder('cpe')
        .innerJoinAndSelect('cpe.vendor', 'vendor', 'vendor.name = :vendor', { vendor })
        .innerJoinAndSelect('cpe.name', 'name', 'name.name = :name', { name })
        .where('cpe.version like :search', { search: `${version}%` })
        .getMany();

      begginingVersionMatch.forEach(i => response.add(JSON.stringify(i)));

      const middleVersionMatch = await this.cpeRepository
        .createQueryBuilder('cpe')
        .innerJoinAndSelect('cpe.vendor', 'vendor', 'vendor.name = :vendor', { vendor })
        .innerJoinAndSelect('cpe.name', 'name', 'name.name = :name', { name })
        .where('cpe.version like :search', { search: `%${version}%` })
        .getMany();

      middleVersionMatch.forEach(i => response.add(JSON.stringify(i)));

      const noVersionMatch = await this.cpeRepository
        .createQueryBuilder('cpe')
        .innerJoinAndSelect('cpe.vendor', 'vendor', 'vendor.name = :vendor', { vendor })
        .innerJoinAndSelect('cpe.name', 'name', 'name.name = :name', { name })
        .where('cpe.version like :search', { search: `%` })
        .getMany();

      noVersionMatch.forEach(i => response.add(JSON.stringify(i)));

      return Array.from(response).map(item => item = JSON.parse(item));

    } else if (vendor && partialName) {

      const response: Set<string> = new Set();

      const foundExact = await this.productNameRepository
        .createQueryBuilder('productName')
        .innerJoinAndSelect('productName.vendor', 'vendor', 'vendor.name = :vendor', { vendor })
        .where('productName.name like :search', { search: partialName })
        .getOne();

      if (foundExact) {
        response.add(JSON.stringify(foundExact));
      }

      const foundStarting = await this.productNameRepository
        .createQueryBuilder('productName')
        .innerJoinAndSelect('productName.vendor', 'vendor', 'vendor.name = :vendor', { vendor })
        .where('productName.name like :search', { search: `${partialName}%` })
        .take(20)
        .getMany();

      foundStarting.forEach(i => response.add(JSON.stringify(i)));

      const foundEnding = await this.productNameRepository
        .createQueryBuilder('productName')
        .innerJoinAndSelect('productName.vendor', 'vendor', 'vendor.name = :vendor', { vendor })
        .where('productName.name like :search', { search: `%${partialName}%` })
        .take(30)
        .getMany();

      foundEnding.forEach(i => response.add(JSON.stringify(i)));

      return Array.from(response).map(item => item = JSON.parse(item));

    } else if (vendor) {

      if (vendor === 'returnAllVendors') {
        return await this.vendorRepository.find();
      }

      const response: Set<string> = new Set();

      const foundStarting = await this.vendorRepository.find({
        where: {
          name: Like(`${vendor}%`),
        },
        take: 20
      });
      foundStarting.forEach(i => response.add(JSON.stringify(i)));

      const foundEnding = await this.vendorRepository.find({
        where: {
          name: Like(`%${vendor}%`),
        },
        take: 30
      });
      foundEnding.forEach(i => response.add(JSON.stringify(i)));

      return Array.from(response).map(item => item = JSON.parse(item));

    } else if (name) {

      if (name === 'returnAllNames') {
        return await this.productNameRepository.find();
      }

      const response: Set<string> = new Set();

      const foundStarting = await this.productNameRepository.find({
        where: {
          name: Like(`${name}%`),
        },
        take: 20
      });
      foundStarting.forEach(i => response.add(JSON.stringify(i)));

      const foundEnding = await this.productNameRepository.find({
        where: {
          name: Like(`%${name}%`),
        },
        take: 30
      });
      foundEnding.forEach(i => response.add(JSON.stringify(i)));

      return Array.from(response).map(item => item = JSON.parse(item));
    } else {
      return [];
    }
  }

  public async create(cpe: CPE, user: User): Promise<ProductShowDTO> {

    const checkCPE = await this.productRepository.findOne({
      where: { cpe }
    });
    if (checkCPE) {
      throw new MySystemError(`Product with such CPE already exist, please try something else !`, 400);
    }

    const newProduct: Partial<Product> = {
      cpe,
      user,
      productCVEs: [],
    };

    const savedProduct: Product = await this.productRepository.save(
      newProduct
    );

    return this.convertToShoWDTO(savedProduct);
  }

  public async toggleAssignCVE(product: Product, cve: CVE): Promise<{ message: string; }> {

    const findProductCVE = await this.productCVERepository.findOne({
      where: {
        productId: product.id,
        cveId: cve.id
      }
    });

    if (!findProductCVE) {
      throw new MySystemError(`Product doesn't have such CVE or no such Product found!`, 400);
    }

    const savedProductCVE = await this.productCVERepository.save({
      ...findProductCVE,
      isAssigned: !findProductCVE.isAssigned
    });
    delete savedProductCVE.productId;
    delete savedProductCVE.cveId;
    delete product.productCVEs;
    savedProductCVE.product = product as any;

    const alertStorage = JSON.parse(fs.readFileSync('./src/database/tasks/alert.json', 'utf8'));

    if (savedProductCVE.isAssigned) {
      alertStorage[new Date().toString().substring(0, 24)] = savedProductCVE;

    } else {
      for (const key in alertStorage) {
        if (alertStorage.hasOwnProperty(key)) {
          const check = JSON.stringify(alertStorage[key]).replace('"isAssigned":true', '"isAssigned":false');
          if (check === JSON.stringify(savedProductCVE)) {
            delete alertStorage[key];
          } else {
            alertStorage[new Date().toString().substring(0, 24)] = savedProductCVE;
          }
        }
      }
    }

    fs.writeFileSync('./src/database/tasks/alert.json', JSON.stringify(alertStorage));

    return { message: 'CVE assign toggled successfully!' };
  }

  public async updateCVEs(): Promise<{ message: string; }> {

    await this.taskService.updateCVEs();

    return { message: 'CVEs updated successfully!' };
  }

  public async updateCPEs(): Promise<{ message: string; }> {

    await this.taskService.updateCPEs();

    return { message: 'CPEs updated successfully!' };
  }

  public convertToShoWDTO(product: any): ProductShowDTO {
    (product.productCVEs as any) = product.productCVEs.map((item: any) => item = { isAssigned: item.isAssigned, cve: item.cve });

    product.title = product.cpe.title;
    product.vendor = product.cpe.vendor.name;
    product.name = product.cpe.name.name;
    product.version = product.cpe.version;
    product.cpeUri23 = product.cpe.cpeUri23;

    return plainToClass(ProductShowDTO, product, {
      excludeExtraneousValues: true
    });
  }

}
