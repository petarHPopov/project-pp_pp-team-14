import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CPE } from '../../database/entities/cpe.entity';
import { CVE } from '../../database/entities/cve.entity';
import { ProductCVE } from '../../database/entities/product-cve.entity';
import { Product } from '../../database/entities/product.entity';
import { ProductName } from '../../database/entities/productName.entity';
import { Update } from '../../database/entities/updates.entity';
import { Vendor } from '../../database/entities/vendor.entity';
import { TaskModule } from '../../database/tasks/tasks.module';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([CPE, CVE, Product, ProductName, ProductCVE,  Update, Vendor]),
    TaskModule,
  ],
  providers: [ProductsService],
  controllers: [ProductsController],
  exports: [ProductsService],
})
export class ProductsModule { }
