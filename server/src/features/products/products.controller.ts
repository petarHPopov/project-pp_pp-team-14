import { Body, Controller, Get, Param, Post, Put, Query, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { UserDecorator } from '../../common/decorators/user.decorator';
import { AdminGuard } from '../../common/guards/admin.guard';
import { AuthGuardWithBlacklisting } from '../../common/guards/blacklist.guard';
import { CPEByURIPipe } from '../../common/pipe/cpe-by-uri.pipe';
import { CVEByIdPipe } from '../../common/pipe/cve-by-id';
import { ProductByIdPipe } from '../../common/pipe/product-by-id.pipe';
import { CPE } from '../../database/entities/cpe.entity';
import { CVE } from '../../database/entities/cve.entity';
import { Product } from '../../database/entities/product.entity';
import { ProductName } from '../../database/entities/productName.entity';
import { User } from '../../database/entities/user.entity';
import { Vendor } from '../../database/entities/vendor.entity';
import { ProductGetAllQueryDTO } from './models';
import { ProductCheckQueryDTO } from './models/products-check-query.dto';
import { ProductShowDTO } from './models/products-show.dto';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  public constructor(private readonly productsService: ProductsService) { }

  @UseGuards(AuthGuardWithBlacklisting)
  @Get()
  @UsePipes(ValidationPipe)
  public async all(@Query() query: ProductGetAllQueryDTO): Promise<ProductShowDTO[]> {

    return await this.productsService.all(
      query.search,
    );
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Get('checkProduct')
  @UsePipes(ValidationPipe)
  public async checkProduct(@Query() query: ProductCheckQueryDTO): Promise<Vendor[] | ProductName[] | CPE[]> {

    return await this.productsService.checkProduct(
      query.name,
      query.partialName,
      query.vendor,
      query.version,

    );
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Get('/:id')
  public async getByCPE(@Param('id', ProductByIdPipe) product: Promise<Product>): Promise<ProductShowDTO> {

    return this.productsService.convertToShoWDTO(await product);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Post()
  @UsePipes(ValidationPipe)
  public async create(@Body('cpeUri23', CPEByURIPipe) cpe: Promise<CPE>, @UserDecorator() user: User): Promise<ProductShowDTO> {

    return await this.productsService.create(await cpe, user);
  }

  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  @Put('/updateCVEs')
  public async updateCVEs(): Promise<{ message: string; }> {

    return this.productsService.updateCVEs();
  }

  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  @Put('/updateCPEs')
  public async updateCPEs(): Promise<{ message: string; }> {

    return this.productsService.updateCPEs();
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Put('/:id')
  public async toggleAssignCVE(
    @Param('id', ProductByIdPipe) product: Promise<Product>,
    @Body('cve', CVEByIdPipe) cve: Promise<CVE>,
  ): Promise<{ message: string; }> {
    return this.productsService.toggleAssignCVE(await product, await cve);
  }

}
