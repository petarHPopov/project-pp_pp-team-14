import { Test, TestingModule } from '@nestjs/testing';
import { CPE } from '../../database/entities/cpe.entity';
import { CVE } from '../../database/entities/cve.entity';
import { Product } from '../../database/entities/product.entity';
import { ProductName } from '../../database/entities/productName.entity';
import { User } from '../../database/entities/user.entity';
import { Vendor } from '../../database/entities/vendor.entity';
import { ProductCheckQueryDTO, ProductGetAllQueryDTO, ProductShowDTO, } from './models';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';

describe('Products Controller', () => {
  let controller: ProductsController;
  const productService: Partial<ProductsService> = {
    all(): Promise<ProductShowDTO[]> {
      return null;
    },
    checkProduct(): Promise<Vendor[] | ProductName[] | CPE[]> {
      return null;
    },
    convertToShoWDTO(): ProductShowDTO {
      return null;
    },
    create(): Promise<ProductShowDTO> {
      return null;
    },
    updateCVEs(): Promise<{ message: string; }> {
      return null;
    },
    updateCPEs(): Promise<{ message: string; }> {
      return null;
    },
    toggleAssignCVE(): Promise<{ message: string; }> {
      return null;
    }
  };
  const mockAllQuery = new ProductGetAllQueryDTO();
  const mockCheckQuery = new ProductCheckQueryDTO();
  const mockProduct = new Product();
  const mockCPE = new CPE();
  const mockCVE = new CVE();
  const mockUser = new User();

  beforeEach(async() => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsController],
      providers: [
        {
          provide: ProductsService,
          useValue: productService,
        },
      ],
    }).compile();

    controller = module.get<ProductsController>(ProductsController);

    mockAllQuery.search = 'test';
    mockCheckQuery.name = 'test1';
    mockCheckQuery.partialName = 'test2';
    mockCheckQuery.vendor = 'test3';
    mockCheckQuery.version = 'test4';

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('Test method all()', () => {
    it('should return correct result', async() => {

      jest
        .spyOn(productService, 'all')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.all(mockAllQuery);

      expect(result).toBe('test');
    });

    it('should call all() metod on productService correctly ', async() => {

      const spy = jest.spyOn(productService, 'all');
      await controller.all(mockAllQuery);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('test');
    });
  });

  describe('Test method checkProduct()', () => {
    it('should return correct result', async() => {

      jest
        .spyOn(productService, 'checkProduct')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.checkProduct(mockCheckQuery);

      expect(result).toBe('test');
    });

    it('should call checkProduct() metod on productService correctly ', async() => {

      const spy = jest.spyOn(productService, 'checkProduct');
      await controller.checkProduct(mockCheckQuery);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('test1', 'test2', 'test3', 'test4');
    });
  });

  describe('Test method getByCPE()', () => {
    it('should return correct result', async() => {

      jest
        .spyOn(productService, 'convertToShoWDTO')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.getByCPE(Promise.resolve(mockProduct));

      expect(result).toBe('test');
    });

    it('should call convertToShowDTO() metod on productService correctly ', async() => {

      const spy = jest.spyOn(productService, 'convertToShoWDTO');
      await controller.getByCPE(Promise.resolve(mockProduct));

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockProduct);
    });
  });

  describe('Test method create()', () => {
    it('should return correct result', async() => {

      jest
        .spyOn(productService, 'create')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.create(Promise.resolve(mockCPE), mockUser);

      expect(result).toBe('test');
    });

    it('should call create() metod on productService correctly ', async() => {

      const spy = jest.spyOn(productService, 'create');
      await controller.create(Promise.resolve(mockCPE), mockUser);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockCPE, mockUser);
    });
  });

  describe('Test method updateCVEs()', () => {
    it('should return correct result', async() => {

      jest
        .spyOn(productService, 'updateCVEs')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.updateCVEs();

      expect(result).toBe('test');
    });

    it('should call updateCVEs() metod on productService correctly ', async() => {

      const spy = jest.spyOn(productService, 'updateCVEs');
      await controller.updateCVEs();

      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

  describe('Test method updateCPEs()', () => {
    it('should return correct result', async() => {

      jest
        .spyOn(productService, 'updateCPEs')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.updateCPEs();

      expect(result).toBe('test');
    });

    it('should call updateCPEs() metod on productService correctly ', async() => {

      const spy = jest.spyOn(productService, 'updateCPEs');
      await controller.updateCPEs();

      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

  describe('Test method toggleAssignCVE()', () => {
    it('should return correct result', async() => {

      jest
        .spyOn(productService, 'toggleAssignCVE')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.toggleAssignCVE(Promise.resolve(mockProduct), Promise.resolve(mockCVE));

      expect(result).toBe('test');
    });

    it('should call toggleAssignCVE() metod on productService correctly ', async() => {

      const spy = jest.spyOn(productService, 'toggleAssignCVE');
      await controller.toggleAssignCVE(Promise.resolve(mockProduct), Promise.resolve(mockCVE));

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockProduct, mockCVE);
    });
  });

});
