import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Post, Put, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { UserDecorator } from '../../common/decorators/user.decorator';
import { AdminGuard } from '../../common/guards/admin.guard';
import { AuthGuardWithBlacklisting } from '../../common/guards/blacklist.guard';
import { User } from '../../database/entities/user.entity';
import { CreateUserDTO, ReturnUserDTO, UserQueryDTO } from './models';
import { UserUpdateDTO } from './models/update-user.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  public constructor(private readonly usersService: UsersService) { }

  @Get(':id')
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public async getUserById(@Param('id') id: string): Promise<ReturnUserDTO> {
    return await this.usersService.getUserById(id);
  }

  @Get()
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public async getAll(@Query() query: UserQueryDTO): Promise<ReturnUserDTO[]> {
    return await this.usersService.getAll(query);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public async createUser(
    @Body() user: CreateUserDTO,
  ): Promise<ReturnUserDTO> {
    return await this.usersService.createUser(user);
  }

  @Put('/alertAdmin')
  @UseGuards(AuthGuardWithBlacklisting)
  public async alertAdmin(): Promise<{ message: string; }> {
    await this.usersService.alertAdmin();
    return {
      message: `Admin was alerted!`,
    };
  }

  @Put('/:id')
  @UseGuards(AuthGuardWithBlacklisting)
  public async updateUserProfilePicture(@Body() body: { img: string; }, @UserDecorator() user: User): Promise<ReturnUserDTO> {
    return await this.usersService.updateUserProfilePicture(body.img, user);
  }

  @Put('/:id/roles')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public async updateUserRoles(@Param('id') userId: string): Promise<ReturnUserDTO> {
    return await this.usersService.updateUserRoles(userId);
  }

  @Put('/:id/update')
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public async updateUserEmail(@Param('id') id: string, @Body() user: UserUpdateDTO): Promise<{ message: string; }> {
    await this.usersService.updateUserEmail(id, user);
    return {
      message: `User was updated!`,
    };
  }

  @Delete('/:id')
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public async deleteUser(@Param('id') id: string): Promise<{ message: string; }> {
    await this.usersService.deleteUser(id);

    return {
      message: `User was deleted!`,
    };
  }
}
