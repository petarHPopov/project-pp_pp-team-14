import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import typeorm = require('typeorm');
import { UserRole } from '../../common/enums/user-role.enum';
import { MockType, repositoryMockFactory } from '../../common/types/mock-type';
import { User } from '../../database/entities/user.entity';
import { TasksService } from '../../database/tasks/tasks.service';
import { CreateUserDTO, UserQueryDTO } from './models';
import { ReturnUserDTO } from './models/return-user.dto';
import { UserUpdateDTO } from './models/update-user.dto';
import { UsersService } from './users.service';

describe('UserService', () => {
  let service: UsersService;
  let userRepositoryMock: MockType<Repository<User>>;
  const tasksService: Partial<TasksService> = {
    alertAdmin(): Promise<void> {
      return null;
    },
  };

  const mockUserQuery = {
    username: 'Test',
    email: 'Test@test.as',
  } as UserQueryDTO;

  let mockUser: User;

  let mockUser2: User;

  const mockReturnUser = {
    id: '2',
    name: 'Admin',
    username: 'Test',
    email: 'Test@test.as',
    role: 'User',
    userProfileImgUrl: 'http://this.jpg'
  } as  ReturnUserDTO;

  const mockCreateUser = {
    username: 'Test',
    password: 'TestP@ss.',
    email: 'Test@test.as',
  } as CreateUserDTO;

  let mockSaveUser: Partial<User>;

  const mockReturnUpdatedRoleUser = {
    role: 'Admin',
  } as  ReturnUserDTO;

  const mockUserUpdate: UserUpdateDTO = {
    email: 'asd@asd.asd',
  };

  beforeEach(async() => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useFactory: repositoryMockFactory,
        },
        {
          provide: TasksService,
          useValue: tasksService,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    userRepositoryMock = module.get(getRepositoryToken(User));

    mockUser = {
      id: '2',
      name: 'Admin',
      username: 'Test',
      password: 'TestP@ss',
      email: 'Test@test.as',
      userProfileImgUrl: 'http://this.jpg',
      role: 'User',
    } as User;

    mockUser2 = {
      id: '3',
      name: 'Admin',
      username: 'Test',
      password: 'testP@ss',
      email: 'Test@test.as',
      userProfileImgUrl: 'http://this.jpg',
      role: 'User',
    } as User;

    mockSaveUser = {
      username: 'Petar',
      password: 'bcryptMock',
      email: 'Petar@the.best',
      role: UserRole.User,
    };

    typeorm.getRepository = jest.fn().mockReturnValue({
      findAndCount: jest
        .fn()
        .mockReturnValueOnce([[], 1])
        .mockReturnValueOnce([[], 2])
        .mockReturnValueOnce([[], 3])
        .mockReturnValueOnce([[], 4])
        .mockReturnValueOnce([[], 5])
        .mockReturnValueOnce([[], 6]),
    });

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(userRepositoryMock).toBeDefined();
  });

  describe('Test metod getAll()', () => {
    it('should return correct result with Query', async() => {
      userRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([mockUser]),
      }));
      userRepositoryMock.find.mockReturnValue([mockUser]);

      const result = await service.getAll(mockUserQuery);

      expect(result).toEqual([mockReturnUser]);
    });

    it('should return correct result without Query', async() => {
      userRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([mockUser]),
      }));
      userRepositoryMock.find.mockReturnValue([mockUser]);

      const result = await service.getAll({} as UserQueryDTO);

      expect(result).toEqual([mockReturnUser]);
    });

    it('should call mockRepository.queryBuilder metod', async() => {
      const spy = jest.spyOn(userRepositoryMock, 'createQueryBuilder');
      userRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([mockUser]),
      }));
      userRepositoryMock.find.mockReturnValue([mockUser]);

      await service.getAll(mockUserQuery);

      expect(spy).toBeCalledTimes(1);
    });

    it('should call mockRepository.find metod', async() => {
      userRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([mockUser]),
      }));
      userRepositoryMock.find.mockReturnValue([mockUser]);
      const spy = jest.spyOn(userRepositoryMock, 'find');

      await service.getAll(mockUserQuery);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        id: In(['2']),
      });
    });
  });

  describe('Test metod getUserById()', () => {
    it('should return correct result', async() => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      const result = await service.getUserById('2');

      expect(result).toEqual(mockReturnUser);
    });

    it('should call mockRepository.findOne metod', async() => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      const spy = jest.spyOn(userRepositoryMock, 'findOne');

      await service.getUserById('7');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        id: '7',
        isDeleted: false,
      });
    });

    it('should throw an Error with specified message', async() => {
      userRepositoryMock.findOne.mockReturnValue(null);
      let test: string;
      try {
        await service.getUserById('4');
      } catch (error) {
        test = error.message;
      }
      expect(test).toBe('User not found!');
    });
  });

  describe('Test metod createUser()', () => {
    it('should return correct result', async() => {
      userRepositoryMock.findOne.mockReturnValue(undefined);
      userRepositoryMock.create.mockReturnValue(mockCreateUser);
      userRepositoryMock.save.mockReturnValue(mockUser);

      const result = await service.createUser(mockCreateUser);

      expect(result).toEqual(mockReturnUser);
    });

    it('should call mockRepository.findOne metod', async() => {
      userRepositoryMock.findOne.mockReturnValue(undefined);
      userRepositoryMock.create.mockReturnValue(mockCreateUser);
      userRepositoryMock.save.mockReturnValue(mockUser);
      const spy = jest.spyOn(userRepositoryMock, 'findOne');

      await service.createUser(mockCreateUser);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        where: {
          username: mockCreateUser.username,
          isDeleted: false,
        },
      });
    });

    it('should call mockRepository.create metod', async() => {
      const userCr = {
        username: 'Stamat',
        password: 'TestP@ss.',
        email: 'Test@test.as',
      };
      userRepositoryMock.findOne.mockReturnValue(undefined);
      userRepositoryMock.create.mockReturnValue(userCr);

      const spy = jest.spyOn(userRepositoryMock, 'create');

      await service.createUser(mockCreateUser);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(mockCreateUser);
    });

    it('should throw an Error with specified message', async() => {
      userRepositoryMock.findOne.mockReturnValue('user');
      let test: string;
      try {
        await service.createUser(mockCreateUser);
      } catch (error) {
        test = error.message;
      }
      expect(test).toBe('User with this username already exist!');
    });
  });

  describe('Test metod updateUserEmail()', () => {
    it('should return correct result', async() => {
      const fakeUser = new UserUpdateDTO();

      jest.spyOn(service, 'updateUserEmail');

      const result = await service.updateUserEmail('2', fakeUser);

      expect(result).toEqual(undefined);
    });

    it('should call mockRepository.findOne metod', async() => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      const spy = jest.spyOn(userRepositoryMock, 'findOne');

      await service.updateUserEmail('2', mockUserUpdate);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({ id: '2' });
    });

    it('should throw na Error when findOne metod returns undefined', async() => {
      userRepositoryMock.findOne.mockReturnValue(undefined);
      let test: string;

      try {
        await service.updateUserEmail('2', mockUserUpdate);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe('No such user found');
    });

  });

  describe('Test metod updateUserRoles()', () => {
    it('should return correct result', async() => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      userRepositoryMock.findOne.mockReturnValue('Admin');

      const result = await service.updateUserRoles('2');

      expect(result).toEqual(mockReturnUpdatedRoleUser);
    });

    it('should call mockRepository.findOne metod', async() => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      userRepositoryMock.find.mockReturnValue('Admin');
      const spy = jest.spyOn(userRepositoryMock, 'findOne');

      await service.updateUserRoles('2');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({ id: '2'});
    });

    it('should throw na Error when findOne metod returns undefined', async() => {
      userRepositoryMock.findOne.mockReturnValue(undefined);
      userRepositoryMock.find.mockReturnValue('Admin');
      let test: string;

      try {
        await service.updateUserRoles('2');
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe('No such user found');
    });
  });

  describe('Test metod deleteUser()', () => {
    it('should call mockRepository.findOne metod with correct data', async() => {
      const spy = jest
        .spyOn(userRepositoryMock, 'findOne')
        .mockReturnValue(mockUser);

      await service.deleteUser('2');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        id: '2',
        isDeleted: false,
      });
    });
    it('should throw na Error when findOne metod returns undefined', async() => {
      userRepositoryMock.findOne.mockReturnValue(undefined);
      let test: string;

      try {
        await service.deleteUser('2');
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`User with such id 2 doesn\'t exists!`);
    });
  });

  describe('Test method alertAdmin()', () => {
    it('should return correct result', async() => {
      jest
        .spyOn(tasksService, 'alertAdmin')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await service.alertAdmin();

      expect(result).toBe('test');
    });

    it('should call alertAdmin() method on tasksService', async() => {

      const spy = jest.spyOn(tasksService, 'alertAdmin');
      await service.alertAdmin();

      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

});
