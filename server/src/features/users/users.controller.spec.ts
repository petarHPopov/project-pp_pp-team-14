import { Test, TestingModule } from '@nestjs/testing';
import { User } from '../../database/entities/user.entity';
import { CreateUserDTO, UserActivityDTO, UserQueryDTO } from './models';
import { ReturnUserDTO } from './models/return-user.dto';
import { UserUpdateDTO } from './models/update-user.dto';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('User Controller', () => {
  let controller: UsersController;
  const userService: Partial<UsersService> = {
    getAll(): Promise<ReturnUserDTO[]> {
      return null;
    },
    getUserById(): Promise<ReturnUserDTO> {
      return null;
    },
    createUser(): Promise<ReturnUserDTO> {
      return null;
    },
    updateUserEmail():  Promise<void> {
      return null;
    },
    deleteUser():  Promise<void> {
      return null;
    },
    updateUserRoles(): Promise<ReturnUserDTO> {
      return null;
    },
    updateUserProfilePicture(): Promise<ReturnUserDTO> {
      return null;
    },
    alertAdmin(): Promise<void> {
      return null;
    },
  };
  const mockQuery = new UserQueryDTO();
  const mockCreate = new CreateUserDTO();
  const mockUser = { id: '3' } as unknown as User;

  beforeEach(async() => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: userService,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('Test metod getAll()', () => {
    it('should return correct result', async() => {

      jest
        .spyOn(userService, 'getAll')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.getAll(mockQuery);

      expect(result).toBe('test');
    });

    it('should call getAll() metod on userService correctly ', async() => {

      const spy = jest.spyOn(userService, 'getAll');
      await controller.getAll(mockQuery);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockQuery);
    });
  });

  describe('Test metod getUserById()', () => {
    it('should return correct result', async() => {
      jest
        .spyOn(userService, 'getUserById')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.getUserById('3');

      expect(result).toBe('test');
    });

    it('should call getUserById() method on userService', async() => {
      const spy = jest.spyOn(userService, 'getUserById');
      await controller.getUserById('3');
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('3');
    });
  });
  describe('Test metod createUser()', () => {
    it('should return correct result', async() => {
      jest
        .spyOn(userService, 'createUser')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.createUser(mockCreate);

      expect(result).toBe('test');
    });

    it('should call createUser() method on userService', async() => {
      const spy = jest.spyOn(userService, 'createUser');
      await controller.createUser(mockCreate);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockCreate);
    });
  });
  describe('Test metod deleteUser()', () => {
    it('should return correct result', async() => {
      const result = await controller.deleteUser('7');

      expect(result).toEqual({ message: `User was deleted!` });
    });

    it('should call deleteUser() method on userService', async() => {
      const spy = jest.spyOn(userService, 'deleteUser');
      await controller.deleteUser('7');

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('7');
    });
  });
  describe('Test metod updateUserRoles()', () => {
    it('should return correct result', async() => {
      jest
        .spyOn(userService, 'updateUserRoles')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.updateUserRoles('3');

      expect(result).toBe('test');
    });

    it('should call updateUserRoles() method on userService', async() => {
      const spy = jest.spyOn(userService, 'updateUserRoles');
      await controller.updateUserRoles('3');

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('3');
    });
  });
  describe('Test metod updateUserEmail()', () => {
       it('should call updateUserEmail() method on userService', async() => {

      const spy = jest.spyOn(userService, 'updateUserEmail');
      await controller.updateUserEmail('3', mockUser);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('3', mockUser);
    });
  });
  describe('Test metod updateUserProfilePicture()', () => {
    it('should return correct result', async() => {
      jest
        .spyOn(userService, 'updateUserProfilePicture')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.updateUserProfilePicture({img: 'test'}, mockUser);

      expect(result).toBe('test');
    });

    it('should call updateUserProfilePicture() method on userService', async() => {

      const spy = jest.spyOn(userService, 'updateUserProfilePicture');
      await controller.updateUserProfilePicture({img: 'test'}, mockUser);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('test', mockUser);
    });
  });

  describe('Test method alertAdmin()', () => {
    it('should return correct result', async() => {
      jest
        .spyOn(userService, 'alertAdmin')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.alertAdmin();

      expect(result).toEqual({'message': 'Admin was alerted!'});
    });

    it('should call alertAdmin() method on tasksService', async() => {

      const spy = jest.spyOn(userService, 'alertAdmin');
      await controller.alertAdmin();

      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
