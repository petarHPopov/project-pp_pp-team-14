import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { In, Repository } from 'typeorm';
import { UserRole } from '../../common/enums/user-role.enum';
import { MySystemError } from '../../common/exceptions/my-system.error';
import { User } from '../../database/entities/user.entity';
import { TasksService } from '../../database/tasks/tasks.service';
import { CreateUserDTO, ReturnUserDTO, UserQueryDTO } from './models';
import { UserUpdateDTO } from './models/update-user.dto';

@Injectable()
export class UsersService {
    public constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        private readonly taskService: TasksService,
    ) { }

    public async createUser(user: CreateUserDTO): Promise<ReturnUserDTO> {
        // tslint:disable-next-line: typedef
        const { username } = user;

        const uniqueUsername = await this.usersRepository.findOne({
            where: {
                username,
                isDeleted: false,
            },
        });

        if (uniqueUsername) {
            throw new MySystemError(
                'User with this username already exist!',
                400,
            );
        }

        const newUser: User = this.usersRepository.create(user);

        newUser.password = await bcrypt.hash(user.password, 10);
        newUser.products = [];

        const saveUser = await this.usersRepository.save(newUser);

        return this.toReturnUserDTO(saveUser);
    }

    public async getUserById(id: string): Promise<ReturnUserDTO> {
        const userFound = await this.usersRepository.findOne({
            id,
            isDeleted: false,
        });

        if (!userFound) {
            throw new MySystemError('User not found!', 404);
        }

        return this.toReturnUserDTO(userFound);
    }

    public async getAll(query: UserQueryDTO): Promise<ReturnUserDTO[]> {
        let repoQuery = this.usersRepository
            .createQueryBuilder()
            .where('user.isDeleted = :del', { del: false });

        if (query.username) {
            repoQuery = repoQuery.andWhere('user.username like :search', {
                search: `%${query.username}%`,
            });
        }
        if (query.email) {
            repoQuery = repoQuery.andWhere('article.title = :email', {
                email: `%${query.email}%`,
            });
        }

        let userIds: any = await repoQuery.getMany();

        userIds = userIds.map((user: User) => user.id);

        const users: User[] = await this.usersRepository.find({
            id: In(userIds),
        });

        return users.map((user: User) => this.toReturnUserDTO(user));
    }

    public async updateUserProfilePicture(img: string, user: User): Promise<ReturnUserDTO> {
        const foundUser: User = await this.usersRepository.findOne(user.id);
        const userReturn: ReturnUserDTO = this.toReturnUserDTO(foundUser);
        return await this.usersRepository.save({
            ...userReturn,
            userProfileImgUrl: img,
        });
    }

    public async deleteUser(id: string): Promise<void> {
        const foundUser = await this.usersRepository.findOne({
            id,
            isDeleted: false,
        });

        if (!foundUser) {
            throw new MySystemError(
                `User with such id ${id} doesn\'t exists!`,
                400,
            );
        }

        foundUser.isDeleted = true;

        await this.usersRepository.save(foundUser);
    }

    public async updateUserRoles(id: string): Promise<ReturnUserDTO> {
        const foundUser: User = await this.usersRepository.findOne({ id });

        if (foundUser === undefined || foundUser.isDeleted) {
            throw new MySystemError('No such user found', 404);
        }

        let entityToUpdate: User;

        foundUser.role === UserRole.Admin
            ? (entityToUpdate = { ...foundUser, role: UserRole.User })
            : (entityToUpdate = { ...foundUser, role: UserRole.Admin });

        const savedUser: User = await this.usersRepository.save(entityToUpdate);

        return this.toReturnUserDTO(savedUser);
    }

    public async updateUserEmail(id: string, user: UserUpdateDTO): Promise<void> {
        const foundUser: User = await this.usersRepository.findOne({ id });

        if (foundUser === undefined || foundUser.isDeleted) {
            throw new MySystemError('No such user found', 404);
        }

        foundUser.email = user.email;
        await this.usersRepository.save(foundUser);
    }

    public async alertAdmin(): Promise<void> {
        return await this.taskService.alertAdmin();
    }

    public toReturnUserDTO(user: User): ReturnUserDTO {
        return {
            id: user.id,
            name: user.name,
            username: user.username,
            email: user.email,
            role: user.role,
            userProfileImgUrl: user.userProfileImgUrl,
        };
    }
}
