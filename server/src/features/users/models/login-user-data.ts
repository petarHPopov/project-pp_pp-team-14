import { IsNotEmpty, IsString, Length, Matches } from 'class-validator';

export class LoginUserDTO {
    @Length(4, 16)
    @IsNotEmpty()
    @IsString()
    public username: string;

    @Length(4, 32)
    @IsNotEmpty()
    @IsString()
    public password: string;
}
