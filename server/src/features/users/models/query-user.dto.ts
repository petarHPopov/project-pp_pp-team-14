import { IsOptional } from 'class-validator';

export class UserQueryDTO {
  @IsOptional()
  public username?: string;
  @IsOptional()
  public email?: string;
}