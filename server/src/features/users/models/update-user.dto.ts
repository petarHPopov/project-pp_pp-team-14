import { IsEmail } from 'class-validator';

export class UserUpdateDTO {

  @IsEmail()
  public email?: string;
}
