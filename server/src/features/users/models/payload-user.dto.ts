import { UserRole } from '../../../common/enums/user-role.enum';

export class UserPayloadDTO {

    public id: string;

    public username: string;

    public role: UserRole;
}
