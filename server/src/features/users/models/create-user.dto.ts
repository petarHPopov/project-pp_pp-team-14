import {
    IsEmail,
    IsNotEmpty,
    IsString,
    Length,
    MaxLength,
} from 'class-validator';

export class CreateUserDTO {
    @Length(5, 35)
    @IsNotEmpty()
    @IsString()
    public name: string;

    @Length(4, 16)
    @IsNotEmpty()
    @IsString()
    public username: string;

    @IsNotEmpty()
    @IsString()
    @IsEmail()
    @Length(10, 60)
    public email: string;

    @Length(4, 32)
    @IsNotEmpty()
    @IsString()
    public password: string;
}
