import { UserRole } from '../../../common/enums/user-role.enum';

export class ReturnUserDTO {
    public id: string;
    public name: string;
    public username: string;
    public email: string;
    public role: UserRole;
    public userProfileImgUrl: string;
}
