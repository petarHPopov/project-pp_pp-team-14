import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { JwtPayload } from '../common/types/jwt-payload';
import { User } from '../database/entities/user.entity';
import { LoginUserDTO } from '../features/users/models';

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];
  public constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

   public async findUserByName(username: string): Promise<User> {

    return await this.userRepository.findOne({ username , isDeleted: false});
  }

   public async validateUser(username: string, password: string): Promise<User> {
    const user = await this.findUserByName(username);
    if (!user) {

      return null;
    }
    const isUserValidated = await bcrypt.compare(password, user.password);

    return isUserValidated ? user : null;
  }

  public async login(loginUser: LoginUserDTO): Promise<{token: string}> {

    const user = await this.validateUser(
      loginUser.username,
      loginUser.password,
    );

    if (!user) {
      throw new UnauthorizedException('Wrong credentials!');
    }

    const payload: JwtPayload = {
      id: user.id,
      username: user.username,
      email: user.email,
      userProfileImgUrl: user.userProfileImgUrl,
      role: user.role,
    };

    return {
      token: await this.jwtService.signAsync(payload),
    };
  }

  public blacklistToken(token: string): void {
    this.blacklist.push(token);
  }

  public isTokenBlacklisted(token: string): boolean {

     return this.blacklist.includes(token);
  }
}
