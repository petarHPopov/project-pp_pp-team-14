import { Test, TestingModule } from '@nestjs/testing';
import { CreateUserDTO } from '../features/users/models';
import { ReturnUserDTO } from '../features/users/models/return-user.dto';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

describe('AuthController', () => {
    let controller: AuthController;

    let authService: any;

    beforeEach(async() => {
      authService = {
        login(): Promise<void> {
          return null;
        },
        blacklistToken(): Promise<void> {
          return null;
        },
      };

      const module: TestingModule = await Test.createTestingModule({
        controllers: [AuthController],
        providers: [
          {
            provide: AuthService,
            useValue: authService,
          },
        ],
      }).compile();

      controller = module.get<AuthController>(AuthController);
    });

    it('should be defined', () => {
      expect(controller).toBeDefined();
    });

    describe('login()', () => {
      it('should call authService login() with the passed user from the client once', async() => {

        const fakeUser = new CreateUserDTO();
        const spy = jest.spyOn(authService, 'login');

        await controller.login(fakeUser);
        expect(spy).toBeCalledWith(fakeUser);
        expect(spy).toBeCalledTimes(1);
      });

      it('should return the result from authService login()', async() => {

        const fakeUser = new CreateUserDTO();
        const result = new ReturnUserDTO();

        jest.spyOn(authService, 'login').mockReturnValue(Promise.resolve(result));

        const actualResult = await controller.login(fakeUser);

        expect(result).toEqual(actualResult);
      });
    });

    describe('logout()', () => {
      it('should call authService blacklistToken() with the passed token from the client once', async() => {

        const fakeToken = 'token';
        const spy = jest.spyOn(authService, 'blacklistToken');

        await controller.logout(fakeToken);

        expect(spy).toBeCalledWith(fakeToken);
        expect(spy).toBeCalledTimes(1);
      });

      it('should call authService blacklistToken() with the passed token from the client once', async() => {

        const fakeToken = 'token';
        jest.spyOn(authService, 'blacklistToken');

        const result = await controller.logout(fakeToken);

        expect(result).toBeDefined();
        expect(result.msg).toBeDefined();
        expect(result.msg.includes('logout')).toEqual(true);
      });
    });
  });
