import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtPayload } from '../common/types/jwt-payload';
import { User } from '../database/entities/user.entity';
import { LoginUserDTO } from '../features/users/models';
import { AuthService } from './auth.service';
describe('AuthService', () => {
  let service: AuthService;

  const userRepo: Partial<Repository<User>> = {};

  let authService: any;
  let jwtService: any;

  beforeEach(async() => {
   authService = {
      validateUser(): Promise<User> {
        return null;
      },
      findUserByName(): Promise<User> {
        return null;
      },
    };

   jwtService = {
      signAsync(): Promise<any> {
        return null;
      },
    };

   const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        { provide: JwtService, useValue: jwtService },
        {
          provide: getRepositoryToken(User),
          useValue: userRepo,
        },
      ],
    }).compile();

   service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {

    expect(service).toBeDefined();
  });

  describe('login()', () => {
    it('should call  validateUser() once with correct user', async() => {

      const validateFakeUser: User = {
        username: 'asd',
        password: 'password',
        userProfileImgUrl: 'mockuserProfileImgUrl',
        email: 'test@test.com',
        role: 'test',
      } as unknown as User;

      const loginFakeUser: LoginUserDTO = {
        username: 'asd',
        password: 'password',
      };

      const validateUserSpy = jest
        .spyOn(service, 'validateUser')
        .mockReturnValue(Promise.resolve(validateFakeUser));

      const fakeToken = 'token';
      jest
        .spyOn(jwtService, 'signAsync')
        .mockReturnValue(Promise.resolve(fakeToken));

      await service.login(loginFakeUser);

      expect(validateUserSpy).toBeCalledWith(
        loginFakeUser.username,
        loginFakeUser.password,
      );
      expect(validateUserSpy).toBeCalledTimes(1);
    });
    it('should callfindUserByName() once with null', async() => {

      const findUserByName = jest
      .spyOn(service, 'findUserByName')
      .mockReturnValue(null);

      expect(service.login(findUserByName as any)).toEqual(Promise.resolve({}));
      expect(findUserByName).toBeCalledTimes(1);
    });
    it('should throw error if user with such username does not exists', async() => {

      const validateUserSpy = jest
        .spyOn(authService, 'validateUser')
        .mockReturnValue(undefined);

      const fakeToken = 'token';
      jest
        .spyOn(jwtService, 'signAsync')
        .mockReturnValue(Promise.resolve(fakeToken));

      expect(service.login(validateUserSpy as any)).rejects.toThrow();
    });

    it('should call jwtService signAsync() once with correct payload', async() => {

      const validateFakeUser: User = {
        id: '1',
        username: 'asd',
        password: 'password',
        userProfileImgUrl: 'mockuserProfileImgUrl',
        email: 'test@test.com',
        role: 'test',
      } as unknown as User;

      const payload: JwtPayload = {
        id: '1',
        username: 'asd',
        userProfileImgUrl: 'mockuserProfileImgUrl',
        email: 'test@test.com',
        role: 'test',
      };

      const loginFakeUser: LoginUserDTO = {
        username: 'asd',
        password: 'password',
      };

      jest
        .spyOn(service, 'validateUser')
        .mockReturnValue(Promise.resolve(validateFakeUser));

      const fakeToken = 'token';
      const signAsyncSpy = jest
        .spyOn(jwtService, 'signAsync')
        .mockReturnValue(Promise.resolve(fakeToken));

      await service.login(loginFakeUser);

      expect(signAsyncSpy).toBeCalledWith(payload);
      expect(signAsyncSpy).toBeCalledTimes(1);
    });

    it('should return the token from the jwtService signAsync()', async() => {

      const validateFakeUser: User = {
        username: 'asd',
        password: 'password',
        userProfileImgUrl: 'mockuserProfileImgUrl',
        email: 'test@test.com',
        role: 'test',
      } as unknown as User;

      const loginFakeUser: LoginUserDTO = {
        username: 'asd',
        password: 'password',
      };

      jest
        .spyOn(service, 'validateUser')
        .mockReturnValue(Promise.resolve(validateFakeUser));

      const fakeToken = 'token';
      jest
        .spyOn(jwtService, 'signAsync')
        .mockReturnValue(Promise.resolve(fakeToken));

      const result = await service.login(loginFakeUser);

      expect(result).toEqual({ token: fakeToken });
    });
  });

  describe('blacklistToken()', () => {
    it('should add the passed token to the blacklist collection', () => {

      const fakeToken = 'token';

      service.blacklistToken(fakeToken);

      expect((service as any).blacklist.includes(fakeToken)).toEqual(true);
    });
  });

  describe('isTokenBlacklisted()', () => {
    it('should return true if the passed token exist in the blacklist collection', () => {

      const fakeToken = 'token';
      (service as any).blacklist.push(fakeToken);

      const result = service.isTokenBlacklisted(fakeToken);

      expect(result).toEqual(true);
    });

    it('should return false if the passed token does not exist in the blacklist collection', () => {

      const fakeToken = 'token';

      const result = service.isTokenBlacklisted(fakeToken);

      expect(result).toEqual(false);
    });
  });
});
