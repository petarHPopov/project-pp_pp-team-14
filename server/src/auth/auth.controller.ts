import { Body, Controller, Delete, Post, UseGuards } from '@nestjs/common';
import { Token } from '../common/decorators/token.decorator';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { LoginUserDTO } from '../features/users/models';
import { AuthService } from './auth.service';

@Controller('session')
export class AuthController {
  public constructor(private readonly authService: AuthService) {}

  @Post()
  public async login(@Body() user: LoginUserDTO): Promise<{token: string}> {
    return await this.authService.login(user);
  }

  @Delete()
  @UseGuards(AuthGuardWithBlacklisting)
  public async logout(@Token() token: string): Promise<{msg: string}> {
    this.authService.blacklistToken(token);
    return {
      msg: 'Successful logout!',
    };
  }
}
