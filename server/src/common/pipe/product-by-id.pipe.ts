import { Injectable, PipeTransform } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { Product } from '../../database/entities/product.entity';
import { MySystemError } from '../exceptions/my-system.error';

@Injectable()
export class ProductByIdPipe implements PipeTransform<string, Promise<Product>> {
  public async transform(value: string): Promise<Product> {
    const connection = getConnection();
    const product = await connection.manager
      .getRepository(Product).findOne(value);

    if (!product) {
      throw new MySystemError('No such Product found!', 404);
    }

    return product;
  }
}
