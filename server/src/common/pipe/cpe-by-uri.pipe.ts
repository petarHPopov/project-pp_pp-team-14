import { Injectable, PipeTransform } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { CPE } from '../../database/entities/cpe.entity';
import { MySystemError } from '../exceptions/my-system.error';

@Injectable()
export class CPEByURIPipe implements PipeTransform<string, Promise<CPE>> {
  public async transform(value: string): Promise<CPE> {
    const connection = getConnection();
    const foundCPE = await connection.manager
      .getRepository(CPE)
      .findOne({ where: { cpeUri23: value } });

    if (!foundCPE) {
      throw new MySystemError('No such CPE found!', 404);
    }

    return foundCPE;
  }
}
