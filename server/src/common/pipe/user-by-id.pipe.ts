import { Injectable, PipeTransform } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { User } from '../../database/entities/user.entity';
import { MySystemError } from '../exceptions/my-system.error';

@Injectable()
export class UserByIdPipe implements PipeTransform<string, Promise<User>> {
  public async transform(value: string): Promise<User> {
    const connection = getConnection();
    const user = await connection.manager
      .getRepository(User)
      .findOne(value, { where: { isDeleted: false } });

    if (!user) {
      throw new MySystemError('No such User found!', 404);
    }
    return user;
  }
}
