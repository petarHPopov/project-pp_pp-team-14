export type MockType<T> = {
    [P in keyof T]: jest.Mock<{}>;
  };

export const repositoryMockFactory = jest.fn(() => ({
    find: jest.fn((entity) => entity),
    findOne: jest.fn((entity) => entity),
    findAndCount: jest.fn((entity) => entity),
    create: jest.fn((entity) => entity),
    innerJoinAndSelect: jest.fn((entity) => entity),
    save: jest.fn((entity) => entity),
    update: jest.fn((entity) => entity),
    delete: jest.fn((entity) => entity),
    createQueryBuilder: jest.fn((entity) => entity),
  }));