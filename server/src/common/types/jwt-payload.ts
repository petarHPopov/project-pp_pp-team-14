export class JwtPayload {
    public id: string;
    public username: string;
    public email: string;
    public userProfileImgUrl: string;
    public role: string;
  }
