import {  ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Response } from 'express';
import { MySystemError } from '../exceptions/my-system.error';

@Catch(MySystemError)
export class MySystemErrorFilter implements ExceptionFilter {
  public catch(exception: MySystemError, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
