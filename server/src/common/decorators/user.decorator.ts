import { createParamDecorator } from '@nestjs/common';

export const UserDecorator = createParamDecorator((_, req) => req.user);
